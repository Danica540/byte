;; Pomocne funkcije za stampanje u unos poruka iz terminala

(defun stampanjePoruke (poruka)
(format t "~%~a~%" poruka))

(defun stampajInlinePoruku (poruka)
(format t "~a" poruka))

(defun unosKorisnika (poruka)
    (format t "~%~a~%" poruka)
    (read)
)

;; aritmeticka progresija
(defun progres (n k)
(cond
    ((> n 0) (append (progres (- n 1) k) (list (+ 1 (* k (- n 1))))))
))

;; generise listu sa elementima od 0 do brojElemenata
(defun generisiListu (brojElemenata)
(cond 
    ((eq brojElemenata 0) '())
    (t (append (list (- brojElemenata 1)) (generisiListu (- brojElemenata 1))))
))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;                                                                                                ;;;;;
;;;;;                                       INFERENCE ENGINE                                         ;;;;;
;;;;;                                                                                                ;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; POMOCNE FUNKCIJE

;; provera da li je parametar s izvorna promenljiva (simbol koji pocinje sa ?)
(defun true-var? (s) 
  (if (symbolp s)
      (equal #\? (char (symbol-name s) 0))
    nil))

;; provera da li je parametar s promenljiva (simbol koji pocinje sa ? ili %)
(defun var? (s) 
  (if (symbolp s)
      (let ((c (char (symbol-name s) 0)))
        (or (equal c #\?) (equal c #\%)))
    nil))

;; provera da li je parametar s funkcija (simbol koji pocinje sa =)
(defun func? (s) 
  (if (symbolp s)
      (equal #\= (char (symbol-name s) 0))
    nil))

;; provera da li je parametar s predefinisani predikat (simbol koji pocinje sa !)
(defun predefined-predicate? (s)
  (if (symbolp s)
      (equal #\! (char (symbol-name s) 0))
    nil))

;; provera da li je parametar s konstanta (ako nije promenljiva ili funkcija onda je konstanta)
(defun const? (s)
  (not (or (var? s) (func? s))))

;; rekurzivna provera da li je parametar f funkcija od parametra x
(defun func-of (f x)
  (cond
   ((null f) ; kraj rekurzije
    t)
   ((atom f)
    (equal f x))
   (t
    (or (func-of (car f) x) (func-of (cdr f) x)))))

;; provera da li funkcija f ima promenljivih
(defun has-var (f)
  (cond
   ((null f) 
    nil)
   ((atom f)
    (var? f))
   (t
    (or (has-var (car f)) (has-var (cdr f))))))

;; funkcija koja vraca konsekvencu pravila
(defun rule-consequence (r)
  (car (last r)))

;; funkcija koja vraca premisu pravila
(defun rule-premises (r)
  (let ((p (cadr r)))
    (if (and (listp p) (equal (car p) 'and))
        (cdr p)
      (list p))))
      
;; funkcija koja vrsi prebacivanje upita u interni format (izbacuje 'and)
(defun format-query (q)
  (if (and (listp q) (equal (car q) 'and))
      (cdr q)
    (list q)))
    
;; izracunavanje istinitosne vrednosti predefinisanog predikata
(defun evaluate-predicate (p ls)
  (if (has-var p) nil  ; ako poseduje slobodne promenljive vraca nil (nije validna situacija)
    (if (eval p) 
        (list ls) ; ako predikat vazi vraca ulaznu listu smena
      nil))) ; u suprotnom vraca nil

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; INTERFEJSNE FUNKCIJE I GLOBALNE PROMENLJIVE

(defparameter *FACTS* nil)
(defparameter *RULES* nil)
(defparameter *MAXDEPTH* 10)

;; priprema *FACTS*, *RULES* i *MAXDEPTH*
(defun prepare-knowledge (lr lf maxdepth)
  (setq *FACTS* lf *RULES* (fix-rules lr) *MAXDEPTH* maxdepth))

;; vraca broj rezulata izvodjenja
(defun count-results (q)
  (length (infer- (format-query q) '(nil) 0)))

;; vraca listu lista smena
(defun infer (q)
  (filter-results (infer- (format-query q) '(nil) 0)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FUNKCIJE KOJE VRSE DODELU NOVIH JEDINSTVENIH PROMENLJIVIH PRAVILIMA

(defun fix-rules (lr)
  (if (null lr) nil
    (cons (fix-rule (car lr)) (fix-rules (cdr lr)))))

(defun fix-rule (r)
  (let ((ls (make-rule-ls r nil)))
    (apply-ls r ls)))

(defun make-rule-ls (r ls)
  (cond
   ((null r)
    ls)
   ((var? r)
    (let ((a (assoc r ls)))
      (if (null a)
          (cons (list r (gensym "%")) ls)
        ls)))
   ((atom r)
    ls)   
   (t
    (make-rule-ls (cdr r) 
                  (make-rule-ls (car r) ls)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FUNKCIJE KOJE VRSE PRIPREMU REZULTATA (IZBACUJU SMENE KOJE SE ODNOSE NA INTERNE PROMENLJIVE)

(defun filter-results (lls)
  (if (null lls) nil
    (cons (filter-result (car lls)) (filter-results (cdr lls)))))

(defun filter-result (ls)
  (if (null ls) nil
    (if (true-var? (caar ls))
        (cons (car ls) (filter-result (cdr ls)))
      (filter-result (cdr ls)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FUNKCIJE KOJE SE KORISTE U IZVODJENJU

;; glavna funkcija za izvodjenje, vraca listu lista smena
;; lq - predikati upita
;; lls - lista listi smena (inicijalno lista koja sadrzi nil)
;; depth - tekuca dubina (inicijalno 0)
(defun infer- (lq lls depth)
  (if (null lq) lls
    (let ((lls-n (infer-q (car lq) lls depth)))
      (if (null lls-n) nil
        (infer- (cdr lq) lls-n depth)))))

;; izvodjenje za jedan predikat iz upita, vraca listu lista smena
(defun infer-q (q lls depth)
  (if (null lls) nil
    (let ((lls-n (infer-q-ls q (car lls) depth)))
      (if (null lls-n)
          (infer-q q (cdr lls) depth)
        (append lls-n (infer-q q (cdr lls) depth))))))

;; izvodjenje za jedan predikat sa jednom listom smena, vraca listu lista smena
(defun infer-q-ls (q ls depth)
  (if (predefined-predicate? (car q))
      (evaluate-predicate (apply-ls q ls) ls)
    (if (< depth *MAXDEPTH*)
        (append (infer-q-ls-lf q *FACTS* ls) (infer-q-ls-lr q *RULES* ls depth))
      (infer-q-ls-lf q *FACTS* ls))))
      
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; izvodjenje nad bazom cinjenica lf, vraca listu lista smena
(defun infer-q-ls-lf (q lf ls)
  (if (null lf) nil
    (let ((ls-n (infer-q-ls-f q (car lf) ls)))
      (if (null ls-n)
          (infer-q-ls-lf q (cdr lf) ls)
        (if (null (car ls-n)) ls-n
          (append ls-n (infer-q-ls-lf q (cdr lf) ls)))))))

;; izvodjenje sa jednom cinjenicom, vraca listu sa listom smena
(defun infer-q-ls-f (q f ls)
  (if (= (length q) (length f)) ; provera na istu duzinu
      (infer-q-ls-f- q f ls)
    nil))

;; izvodjenje sa jednom cinjenicom, vraca listu sa listom smena
(defun infer-q-ls-f- (q f ls)
  (if (null q) (list ls)
    (let ((nq (apply-and-eval (car q) ls)) (nf (car f)))
      (if (var? nq) 
          (infer-q-ls-f- (cdr q) (cdr f) (append ls (list (list nq nf))))
        (if (equal nq nf) 
            (infer-q-ls-f- (cdr q) (cdr f) ls)
          nil)))))
          
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; izvodjenje nad bazom pravila, vraca listu lista smena
(defun infer-q-ls-lr (q lr ls depth)
  (if (null lr) nil
    (let ((ls-n (infer-q-ls-r q (car lr) ls depth)))
      (if (null ls-n)
          (infer-q-ls-lr q (cdr lr) ls depth)
        (if (null (car ls-n)) ls-n
          (append ls-n (infer-q-ls-lr q (cdr lr) ls depth)))))))

;; izvodjenje sa jednim pravilom, vraca listu sa listom smena
(defun infer-q-ls-r (q r ls depth)
  (let ((c (rule-consequence r)))
    (if (= (length q) (length c))
        (let ((lsc (unify q c nil ls)))
          (if (null lsc) nil
            (infer- (apply-ls (rule-premises r) (car lsc)) (cdr lsc) (1+ depth))))
      nil)))

;; unifikacija predikata upita q i konsekvence pravila c primenom liste smena ls, vraca listu smena
(defun unify (q c uls ls)
  (if (or (null q) (null c))
      (if (and (null q) (null c)) (list uls ls) nil)
    (let ((eq (car q)) (ec (car c)))
      (cond
       ((equal eq ec)
        (unify (cdr q) (cdr c) uls ls))
       ((var? eq)
        (cond
         ((var? ec)
          (let ((a (assoc ec uls)))
            (cond
             ((null a)              
              (unify (cdr q) (cdr c) (cons (list ec eq) uls) ls))
             ((equal (cadr a) eq)
              (unify (cdr q) (cdr c) uls ls))
             (t
              nil))))
         ((func? ec)
          nil)
         (t ;; const
          (let ((a (assoc eq ls)))
            (cond
             ((null a)
              (unify (cdr q) (cdr c) uls (cons (list eq ec) ls)))
             ((equal (cadr a) ec)
              (unify (cdr q) (cdr c) uls ls))
             (t 
              nil))))))
       ((func? eq)
        (cond
         ((var? ec)
          (if (func-of eq ec) nil
            (let ((a (assoc ec uls)))
              (cond
               ((null a)              
                (unify (cdr q) (cdr c) (cons (list ec eq) uls) ls))
               ((equal (cadr a) eq)
                (unify (cdr q) (cdr c) uls ls))
               (t
                nil)))))
         ((func? ec)
          nil)
         (t ;; const
          (let ((f (apply-ls eq ls)))
            (if (has-var f) nil
              (if (equal (eval f) ec)
                  (unify (cdr q) (cdr c) uls ls)
                nil))))))
       (t ;; const
        (cond
         ((var? ec)
          (let ((a (assoc ec uls)))
            (cond
             ((null a)              
              (unify (cdr q) (cdr c) (cons (list ec eq) uls) ls))
             ((equal (cadr a) eq)
              (unify (cdr q) (cdr c) uls ls))
             (t
              nil))))
         (t ;; func or const
          nil)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PRIMENA LISTE SMENA I IZRACUNAVANJE IZRAZA

(defun apply-and-eval (x ls)
  (if (var? x)
      (apply-ls x ls)
    (if (and (listp x) (func? (car x)))
        (eval (apply-ls x ls)) 
      x)))

;; primena liste smena ls na izraz x
(defun apply-ls (x ls)
  (cond
   ((null x)
    x)
   ((var? x)
    (let ((ax (assoc x ls)))
      (if (null ax) x
        (cadr ax))))
   ((atom x)
    x)
   (t
    (cons (apply-ls (car x) ls) (apply-ls (cdr x) ls)))))


;; Funkcije za proveru pobede

(defun izracunajBrojStekova (dimenzija)
(/ (izracunajBrojFigura dimenzija) 8))

(defun izracunajBrojFigura (dimenzija)
(/ (* dimenzija (- dimenzija 2)) 2))

;; Funkcija koja proverava da li je stek popunjen
(defun daLiJeStekPopunjen (stek)
(cond 
    ((eq (length (remove '- stek)) 8) T)
    (t NIL)
))


(defun skloni (tabla polje) 
(cond ((eq (length (remove '- (vratiPoljeMatrice (car polje) (cadr polje) tabla))) 8) (ocistiPolje tabla polje))
        (t tabla))
)

;;skloni odredjuje da li imamo pun stek koga treba ukloniti sa polja
;;vraca tablu sa novim stanjem
(defun ocistiPolje (tabla polje)
(cond((eq (car polje) 1) (cons (skloniSaPolja (car tabla) polje 8) (cdr tabla)))
(t (cons (car tabla) (ocistiPolje (cdr tabla) (list (- (car polje) 1) (cadr polje))) ))
))

;;postavlja vrednost polja na prazno
(defun promeniSkor (tabla skor polje)
(let ((stek (vratiPoljeMatrice (car polje) (cadr polje) tabla)))
(cond ((eq (length (remove '- stek)) 8) (noviSkor (cadr stek) skor))
(t skor))
))

(defun noviSkor (figura skor)
(cond((eq figura 'x) (list (1+ (car skor)) (cadr skor)))
(t (list (car skor) (1+ (cadr skor))))
))

(defun div (x y) 
(
/(- x (mod x y)) y
))

;; vraca ko je sledeci na potezu
(defun vratiSledecegIgraca (igrac)
    (if (equal igrac 'X) 'O 'X))

;;; Funkcije za kreiranje i transformisanje table

(defun napraviTablu (brojVrsta brojKolona)
(cond
    ((eq brojVrsta 0) '())
    ( (eq brojVrsta 1) (append (list (reverse (napraviVrstu brojKolona '-))) (napraviTablu (- brojVrsta 1) brojKolona)))
    ((eq brojVrsta brojKolona) (append (list (napraviVrstu brojKolona '-)) (napraviTablu (- brojVrsta 1) brojKolona)))
    ((eq (mod brojVrsta 2) 1) (append (list (reverse (napraviVrstu brojKolona 'X))) (napraviTablu (- brojVrsta 1) brojKolona)))
    (t (append (list (napraviVrstu brojKolona 'O)) (napraviTablu (- brojVrsta 1) brojKolona)))
))

(defun napraviVrstu (brojKolona simbol)
(cond
    ((eq brojKolona 0) '())
    ((eq (mod brojKolona 2) 0) (append (list (napraviCrnoPolje 9 simbol)) (napraviVrstu (- brojKolona 1) simbol)))
    (t (append (list NIL) (napraviVrstu (- brojKolona 1) simbol)))
))

(defun napraviCrnoPolje (visinaSteka simbol)
(cond 
    ((eq visinaSteka 0) '())
    ((eq visinaSteka 1) (append (list simbol) (napraviCrnoPolje (- visinaSteka 1) simbol)))
    (t (append (list '-) (napraviCrnoPolje (- visinaSteka 1) simbol)))
))

;; Funkcija za pribavljanje polja
(defun vratiPoljeMatrice (vrsta kolona matrica)
(cond
    ((null matrica) '())
    ((eq vrsta 1) (vratiPoljeUVrsti kolona (car matrica))) ;; to je ta vrsta
    (t (vratiPoljeMatrice (- vrsta 1) kolona (cdr matrica)))
))

(defun vratiPoljeUVrsti (kolona vrsta)
(cond
    ((null vrsta) '()) ;; nije polje sa indeksom kolone nadjeno u vrsti
    ((eq kolona 1) (car vrsta)) ;; vrati polje ako je to ta kolona
    (t (vratiPoljeUVrsti (- kolona 1) (cdr vrsta)))
))



;;minmax sa alfa beta odsecanjem
;;(nadjiPotezeZaSvaPopunjenaPolja (listaKorisnikovihPolja tabla igrac (nadjiPolja (length tabla) 1)) igrac tabla)
(defun minmaxAB (tabla dubina a b igrac originalIgrac)
    (let 
        ((listaPoteza (vratiSvePotezeSaTable tabla igrac))
        (bestvalue '()) (najboljiPotez '()) (state '())
        (tmp '()) (alpha a) (beta b))
        (cond 
            ((or (null listaPoteza) (= dubina 0)) (setf bestvalue (evaluirajStanje tabla originalIgrac)))
            ((equal igrac 'X) ;X plays the move - Maximizing igrac
                (progn
                    (setf bestvalue -999999)
                    (loop for potez in listaPoteza do
                        (progn 
                            (setf state (odigraj tabla (konvertujpotez potez) (prenos tabla (konvertujpotez potez))))
                            (setf tmp (minmaxAB state (- dubina 1) alpha beta 'O originalIgrac))
                            (if (> (car tmp) bestvalue) (progn (setf bestvalue (car tmp)) (setf najboljiPotez potez)))
                            (setf alpha (max alpha bestvalue))
                            (if (>= alpha beta) (return-from minmaxAB (list bestvalue najboljiPotez))))
                    )
                )
            )
            ((equal igrac 'O) ;O plays the move - Minimizing igrac
                (progn
                    (setf bestvalue 999999)
                    (loop for potez in listaPoteza do
                        (progn
                            (setf state (odigraj tabla (konvertujpotez potez) (prenos tabla (konvertujpotez potez))))
                            (setf tmp (minmaxAB state (- dubina 1) alpha beta 'X originalIgrac))
                            (if (< (car tmp) bestvalue) (progn (setf bestvalue (car tmp)) (setf najboljiPotez potez)))
                            (setf beta (min beta bestvalue))
                            (if (>= alpha beta) (return-from minmaxAB (list bestvalue najboljiPotez))))))
            )) 
            (list bestvalue najboljiPotez)
    )
)

;; ============================== EVALUACIJA ===============================

;; equal
(defun !eq (x y)
    (eq x y))

;; not equal
(defun !ne (x y)
    (not (eq x y)))

(defun evaluirajStanje (tabla igrac)
    (prepare-knowledge (odrediPravila) (odrediCinjenice tabla igrac) 10) 
    (cond 
    ((eq igrac 'X) (+  
        (cadaar (infer '(Evaluate ?value 'move 'X)))
        (cadaar (infer '(Evaluate ?value 'victory 'X)))
        (cadaar (infer '(Evaluate ?value 'neighbour 'X)))))
((eq igrac 'O) (+  
        (cadaar (infer '(Evaluate ?value 'move 'O)))
        (cadaar (infer '(Evaluate ?value 'victory 'O)))
        (cadaar (infer '(Evaluate ?value 'neighbour 'O)))
))
    )
    )

(defun odrediPravila ()
    '(
            (if (Ready-for-evaluation ?value ?param ?igrac)
                then (Evaluate ?value ?param ?igrac))

            (if (and (Check-neighbours ?value) (!eq ?param 'neighbour))
                then (Ready-for-evaluation ?value ?param ?igrac))
            
            (if (and (Figure-difference ?value) (!eq ?param 'move))
                then (Ready-for-evaluation ?value ?param ?igrac))

            (if (and (Stack-X ?value) (!eq ?param 'victory) (!eq ?igrac 'X) (!ne ?value nil))
                then (Ready-for-evaluation +10000 ?param ?igrac))

            (if (and (Stack-X ?value) (!eq ?param 'victory) (!eq ?igrac 'X) (!eq ?value nil))
                then (Ready-for-evaluation 0 ?param ?igrac))

            (if (and (Stack-O ?value) (!eq ?param 'victory) (!eq ?igrac 'O) (!ne ?value nil))
                then (Ready-for-evaluation -10000 ?param ?igrac))

            (if (and (Stack-O ?value) (!eq ?param 'victory) (!eq ?igrac 'O) (!eq ?value nil))
                then (Ready-for-evaluation 0 ?param ?igrac))
           ))

(defun odrediCinjenice (tabla igrac)
  (list
        (list 'Check-neighbours (proveriSusede tabla igrac))
        (list 'Figure-difference (izracunajRazlikuBrojaFigura tabla 13))
        (list 'Stack-X (proveriDaLiPostojiStekIgraca tabla 'X))
        (list 'Stack-O (proveriDaLiPostojiStekIgraca tabla 'O))
))

(defun izracunajRazlikuBrojaFigura (tabla faktor)
    (* (- (izracunajBrojVrsnihFigura tabla 'X) (izracunajBrojVrsnihFigura tabla 'O)) faktor (izracunajFaktor tabla)))

(defun izracunajFaktor (tabla)
(div (* (length tabla) 4) (brojStekova tabla))
)

;;provera borj figura igraca na vrhu
(defun izracunajBrojVrsnihFigura (tabla igrac)
(cond 
    ((null tabla) 0)
    (t (+ (brojVrsnihFiguraUVrsti (car tabla) igrac) (izracunajBrojVrsnihFigura (cdr tabla) igrac ) ))
)
)

(defun brojVrsnihFiguraUVrsti (vrsta igrac)
(cond 
    ((null vrsta) 0)
    (t (+ (proveriFiguruNaVrhu (car vrsta) igrac) (brojVrsnihFiguraUVrsti (cdr vrsta) igrac) ))
)
)

(defun proveriFiguruNaVrhu (polje igrac)
(cond
    ((eq (car (remove '- polje)) igrac) 1)
    (t 0)
)
)

;; provera suseda
(defun proveriSusede (tabla igrac)
     (+ 
            (vratiEvaluacijuSuseda (odigrajSvePoteze tabla (vratiSledecegIgraca igrac)) igrac)
            (proveriTrenutnoStanje tabla igrac)
          )
)

(defun brojStekova (tabla)
(cond 
    ((null tabla) 0)
    (t (+ (brojStekovaUVrsti (car tabla)) (brojStekova (cdr tabla))))
)
)

(defun brojStekovaUVrsti (vrsta)
(cond 
    ((null vrsta) 0)
    (t (+ (stekNaPolju (car vrsta))  (brojStekovaUVrsti (cdr vrsta))) )
)
)

(defun stekNaPolju (polje)
(cond 
    ((> (length (remove '- polje)) 0) 1)
    (t 0)
)
)

(defun proveriTrenutnoStanje (tabla igrac)
(cond
    ((and (proveriDaLiPostojiStekIgraca tabla igrac) (eq igrac 'X)) 300)
    ((and (proveriDaLiPostojiStekIgraca tabla (vratiSledecegIgraca igrac)) (eq igrac 'X)) -300)
    ((and (proveriDaLiPostojiStekIgraca tabla igrac) (eq igrac 'O)) -300)
    ((and (proveriDaLiPostojiStekIgraca tabla (vratiSledecegIgraca igrac)) (eq igrac 'O)) 300)   
    (t 0)
)
)

(defun vratiEvaluacijuSuseda (sveTable igrac)
(cond 
    ((null sveTable) 0)
    ((and (proveriDaLiPostojiStekIgraca (car sveTable) igrac) (eq igrac 'X)) (+ 100 (vratiEvaluacijuSuseda (cdr sveTable) igrac)))
    ((and (proveriDaLiPostojiStekIgraca (car sveTable) igrac) (eq igrac 'O)) (- 100 (vratiEvaluacijuSuseda (cdr sveTable) igrac)))
    ((and (proveriDaLiPostojiStekIgraca (car sveTable) (vratiSledecegIgraca igrac)) (eq (vratiSledecegIgraca igrac) 'X)) (+ (vratiEvaluacijuSuseda (cdr sveTable) igrac) 100))
    ((and (proveriDaLiPostojiStekIgraca (car sveTable) (vratiSledecegIgraca igrac)) (eq (vratiSledecegIgraca igrac) 'O)) (- (vratiEvaluacijuSuseda (cdr sveTable) igrac) 100))
    (t (+ 0 (vratiEvaluacijuSuseda (cdr sveTable) igrac)))
))

;; proverava za svako polje da li se nalazi stek na njemu i koja se vrsna figura
(defun proveriDaLiPostojiStekIgraca(tabla igrac)
(cond
    ((null tabla) NIL)
    ((eq (proveriStekUVrsti (car tabla) igrac) T) T)
    (t (proveriDaLiPostojiStekIgraca (cdr tabla) igrac))
))

(defun proveriStekUVrsti ( vrsta igrac)
(cond 
    ((null vrsta) NIL)
    ((eq (postojiStek (car vrsta) igrac) T) T)
    (t (proveriStekUVrsti (cdr vrsta) igrac))
))

(defun postojiStek (polje igrac)
(cond 
    ((and (eq (length (remove '- polje)) 8) (eq (nth 1 polje) igrac)) T)
    (t NIL)
))

;------------------------- ZAVRSEN DEO SA EVALUACIJOM -----------------------------------------


;;(stampanjePoruke (transformisiNajkracePuteve (vratiValidnePotezeNaOsnovuNajkracegRastojanja '() (vratiPoljeMatrice 1 3 *tablaZaPobedu*) 1 3 0 *tablaZaPobedu*)))
(defun vratiValidnePoteze (tabla indeksVrste indeksKolone)
(obrisiDuplikate (transformisiNajkracePuteve (vratiValidnePotezeNaOsnovuNajkracegRastojanja '() (vratiPoljeMatrice indeksVrste indeksKolone tabla) indeksVrste indeksKolone 0 tabla))))

;; Funkcija za nalazenje validnih poteza na osnovu najkraceg puta do igrac
(defun vratiValidnePotezeNaOsnovuNajkracegRastojanja (trenutnaPutanja trenutnoPolje indeksVrste indeksKolone metrika tabla)
(cond
    ((eq (daLiJePoljeObidjeno trenutnaPutanja indeksVrste indeksKolone) T) '()) ;; uslov za izlazak je da ne obilazimo 2 puta isto polje
    ((or (<= indeksKolone 0) (> indeksKolone (length tabla))) '()) ;; ako polje prelazi granice tabla izadji
    ((or (<= indeksVrste 0) (> indeksVrste (length tabla))) '()) ;; ako polje prelazi granice tabla izadji
    ((> metrika (length tabla)) '())
    ((and 
        (> metrika 0) ;; nije prvi prolaz
        (eq (daLiJeStekPopunjen trenutnoPolje) T)
    ) '())
    ((and 
        (> metrika 0) ;; nije prvi prolaz
        (and 
            (eq (daLiPoljeSadrziFiguru trenutnoPolje) T) 
            (eq (daLiJeStekPopunjen trenutnoPolje) NIL)
        )
    )
    (list (reverse (dodajUListu (list (append (list (append (list indeksVrste) (list indeksKolone))) (list metrika ))) trenutnaPutanja)))) ;; ako naidjes na polje gde mozes da prebacis figuru vrati taj put
    (t (let ((PUTANJA (dodajUListu (list (append (list (append (list indeksVrste) (list indeksKolone))) (list metrika ))) trenutnaPutanja)))
        (append 
            (append 
                (vratiValidnePotezeNaOsnovuNajkracegRastojanja 
                    PUTANJA
                    (vratiPoljeMatrice (- indeksVrste 1) (- indeksKolone 1) tabla) 
                    (- indeksVrste 1) 
                    (- indeksKolone 1) 
                    (+ metrika 1)
                    tabla)
                (vratiValidnePotezeNaOsnovuNajkracegRastojanja 
                    PUTANJA
                    (vratiPoljeMatrice (- indeksVrste 1) (+ indeksKolone 1) tabla) 
                    (- indeksVrste 1) 
                    (+ indeksKolone 1) 
                    (+ metrika 1)
                    tabla)
            )
            (append 
                (vratiValidnePotezeNaOsnovuNajkracegRastojanja 
                    PUTANJA
                    (vratiPoljeMatrice (+ indeksVrste 1) (- indeksKolone 1) tabla) 
                    (+ indeksVrste 1) 
                    (- indeksKolone 1) 
                    (+ metrika 1)
                    tabla)
                (vratiValidnePotezeNaOsnovuNajkracegRastojanja 
                    PUTANJA
                    (vratiPoljeMatrice (+ indeksVrste 1) (+ indeksKolone 1) tabla) 
                    (+ indeksVrste 1) 
                    (+ indeksKolone 1) 
                    (+ metrika 1)
                    tabla)
            )
        )
    ))
))

;; Proverava da li je polje sa indeksom vrste i indeksom kolone vec obidjeno, tj da li se vec nalazi u prosledjenoj listi
;; trenutna putanja je oblika: ( ((1 2) 4) ((1 1) 1) ((2 2) 1) ((1 3) 2) )
(defun daLiJePoljeObidjeno (trenutnaPutanja indeksVrste indeksKolone)
(cond
      ((null trenutnaPutanja) NIL)
      ((equal (caar trenutnaPutanja) (append (list indeksVrste) (list indeksKolone))) T)
      (t (daLiJePoljeObidjeno (cdr trenutnaPutanja) indeksVrste indeksKolone))
))

;; ako je polje vec obidjeno ne dodaje u listu, ako polje nije obidjeno vraca listu sa elementom
(defun dodajUListu (element lista) 
(cond
    ((equal (daLiJePoljeObidjeno lista (caar element) (cadr element)) T) lista)
    (t (append element lista))
))

;; Funkcija koja proverava da li je polje prazno ili sadrzi makar jednu figuru
(defun daLiPoljeSadrziFiguru (trenutnoPolje)
(cond
    ((eq (length (remove '- trenutnoPolje)) 0) NIL)
    (t T)
))

;; Funkcija koja izdvaja polja koja imaju metriku 1 i validna su
(defun transformisiNajkracePuteve (lista) 
(vratiNajblizePuteve lista (nadjiMinimumDuzinePuta lista)))

(defun nadjiMinimumDuzinePuta (lista)
(cond
    ((null (cdr lista)) (length (car lista)))
    (t (min (length (car lista)) (nadjiMinimumDuzinePuta (cdr lista))))
))

(defun vratiNajblizePuteve (lista min)
(cond
    ((null lista) '())
    ((eq (length (car lista)) min) (append (list (cadar lista)) (vratiNajblizePuteve (cdr lista) min)))
    (t (append '() (vratiNajblizePuteve (cdr lista) min)))
))

(defun obrisiDuplikate (listaPuteva)
(cond ((null listaPuteva) '())
      (t (remove-duplicates listaPuteva :test 'equal :from-end t))
)
)

;;--------- funkcije za pomeranje figura na tabli

(defun konvertujIndeksPolja (polje)
(list ( vratiBrojIndeksaVrste (car polje)) (cadr polje)))

;;(konvertujIndeksPolja polje)
;;Prima jedan parametar oblika *(A 4)*, polje na tabli
;;vraca konvertovanu oznaku polja za igru u obliku *(1 4)* 

(defun konvertujPotez (praviPotez)
(append (list (konvertujIndeksPolja (car praviPotez)) (konvertujIndeksPolja (cadr praviPotez))) (last praviPotez)))

;;(konvertujPotez praviPotez)
;;prima jedan argument tipa praviPotez *((A 3)(B 2) 5)*
;;vraca potez sa promenjenim indeksim *((1 3)(2 2) 5)*

(defun prenos (tabla potez)
(listaZaPrenos (remove '- (vratiPoljeMatrice (caar potez) (cadar potez) tabla)) (caddr potez)))

;;(prenos tabla potez)
;; dva argumenta tabla i potez u konvertovano obliku
;;vraca prenos za zadati potez u obliku liste *(x o x)*

(defun listaZaPrenos (stek visina)
(cond ((<= (length stek) visina) '())
      (t (cons (car stek) (listaZaPrenos (cdr stek) visina)))
))

;;(listaZaPrenos stek visina)
;;prima dva argumenta *(x o x o x x) 2* prvi je stek sa koga se uzima drugi je visina sa koje se uzima (krece od 0)
;;vraca listu karaktera koji se prenose *(x o x o)*


(defun smanjiRedUPotezu (potez)
(list (list (- (caar potez) 1) (cadar potez)) (list (- (caadr potez) 1) (cadadr potez)) (caddr potez))
)

;;(smanjiredupotezu potez)
;; smanjuje vrednost reda poteza *((8 4) (7 5) 5)* -> *((7 4) (6 5) 5)*


(defun odigraj (tabla potez prenos)
(cond((and (< (caar potez) 1) (< (caadr potez) 1)) tabla)
     ((eq (caar potez) 1) (cons (skloniSaPolja (car tabla) (car potez) (length prenos)) (odigraj (cdr tabla) (smanjiRedUPotezu potez) prenos)))
     ((eq (caadr potez) 1) (cons (dodajNaPolje (car tabla) (cadr potez) prenos) (odigraj (cdr tabla) (smanjiRedUPotezu potez) prenos)))
     (t (cons (car tabla) (odigraj (cdr tabla) (smanjiRedUPotezu potez) prenos)))
))

;;(odigraj tabla potez prenos)
;;prima 3 argumenta 1.tabla, 2.potez u obliku *((7 4) (6 5) 5)*, 3.prenos u obliku liste (x o x o)
;;vraca tablu sa novonastalim stanjem


(defun dodajNaPolje (red polje prenos)
(cond((eq (cadr polje) 0) red)
      ((eq (cadr polje) 1) (cons (dodaj (car red) prenos) (cdr red)))
      (t (cons (car red) (dodajNaPolje (cdr red) (list (car polje) (- (cadr polje) 1)) prenos)))
))

;;(dodajNapolje red polje prenos)
;;prima tri argumenta 1.jedan red tabla 2.polje u tom redu 3.listu 
;;vraca red tabele sa azuriranim poljem


(defun dodaj (stek prenos)
(dopuni (append prenos (remove '- stek))))

;;(dodaj stek prenos)
;;dva argumenta jedan je stek koji se dopunjuje (- - - - - x x o x) drugi je prenos koji se dodaje (x o x)
;;vraca stek sa novim stanjem (- - x o x x x o x)

(defun dopuni (polje)
(cond((eq (length polje) 9) polje)
      (t (dopuni (cons '- polje)))))
;;(dopuni polje)
;;jedan argument polje bez "-" *(x o o x o)*
;;vraca polje duzine 9 sa "-" karakterom *(- - - - x o o x o)*

(defun skloniSaPolja (red polje br)
(cond((eq (cadr polje) 0) red)
      ((eq (cadr polje) 1) (cons (obrisi (car red) br) (cdr red)))
      (t (cons (car red) (skloniSaPolja (cdr red) (list (car polje) (- (cadr polje) 1)) br)))
))

;;(sklonisapolja red polje br)
;;prima tri argumenta prvi je red tabla *((polje)(polje)....)* drugi argument je tacno polje sa koga se uklanjaju igrac *(1 2)*
;;treci argument je broj plocica koje se uklanjaju *4*
;;vraca jedan red tabla sa azuriranim jednim poljem sa koga su skinute plocice 
;;(skloniSaPolja '( NIL (- - - - - - - - -) NIL (- - O O O X O O X) NIL (- - - - - - - - X) NIL (- - - - - - - - X)) '( 1 4) '3) test primer 


(defun obrisi (polje br)
(cond ((eq br 0) polje)
      ((eq (car polje) '-) (append '(-) (obrisi (cdr polje)  br))) 
      (t (append '(-) (obrisi (cdr polje) (- br 1)))))
)

;;(obrisi polje br)
;;prima dva argumenta polje i br=broj plocica koje treba da se sklone sa polja
;;vraca polje bez skinutih plocica *((- - - x x o o x x) 4)* -> *(- - - - - - - x x)*




(defun nadjiPolja (duzina k)
(cond((> k duzina) '())
     ((eq 0 (mod k 2)) (append (nadjiBrojeve k duzina 2) (nadjiPolja duzina (1+ k))))
     (t (append (nadjiBrojeve k duzina 1) (nadjiPolja duzina (1+ k))))
)
)


(defun nadjiBrojeve (brojV duzina ind)
(cond ((> ind duzina) '())
      (t (cons (list brojV ind) (nadjiBrojeve brojV duzina (+ 2 ind))))
))


(defun postojiNaPoljuPlocicaOdIgraca (zapisPolja tabla igrac) 
(let ((POLJE (vratiPoljeMatrice (car zapisPolja) (cadr zapisPolja) tabla) ))
(and (< 0 (length (remove '- POLJE))) (member igrac POLJE ))
)
)

;; (postojiNaPoljuPlocicaOdIgraca zapisPolja tabla igrac)
;; trazi da li na zadatom polju postoji plocica tog igraca 
;; vraca vrednost T ako na polju postoji igraceva plocica

(defun listaKorisnikovihPolja (tabla igrac listaPolja)
(cond((null listaPolja) '())
     ((postojiNaPoljuPlocicaOdIgraca (car listaPolja) tabla igrac) (append (list (car listaPolja)) (listaKorisnikovihPolja tabla igrac (cdr listaPolja) )))
     (t (listaKorisnikovihPolja tabla igrac (cdr listaPolja) ))
)
)
;; (listaKorisnikovihPolja tabla igrac listaPolja)
;; vraća listu svih polja na kojima ima korisnikovih figura u obliku ((2 3)(3 5)(4 7))


;;; Glavna funkcije za stampanje table
;; Tabla se prvo normalizuje pa transformise za stampanje
;; Parametar koji se prosledjuje ovoj funkciji je obicna reprezentacija table (ne normalizovana, niti transformisana)
;; Ona vrsi normalizaciju i transformaciju matrice, i takvu matricu salje na stampanje pomocnoj funkciji --stampajTablu
(defun stampajTablu (tabla)
(--stampajTablu (transformisiTabluZaStampanje(normalizujTabluZaStampanje tabla))))

;; Pomocna funkcija za stampanje table, prvo stampa brojeve kolona, pa novi red, pa vrste
;; Prosledjuje joj se transformisana tabla, spremna za stampanje
(defun --stampajTablu (tabla)
(progn 
    (format t "  ") 
    (stampajInlinePoruku (stampajZaglavljeZaKolone (progres (length tabla) 4) (reverse (generisiListu (* 4 (length tabla)))))) 
    (format t "~%") 
    (--stampajVrste tabla 0 0))
)

;; U ovoj funkciji je svaka vrsta table indeksirana indeksVrste promenljivom
;; svaka vrsta ovakve table ima 3 reda vrste, ta 3 red ase indeksiraju parametrom indeks, koji uzima vrednosti 0, 1 i 2
(defun --stampajVrste (tabla indeks indeksVrste)
(cond
    ((null tabla) " ")
    (t (progn 
            (--stampajVrstu (car tabla) indeks indeksVrste)
            (--stampajVrste (cdr tabla) indeks (+ indeksVrste 1))))
))

;; Funkcija koja stampa svaki red vrste, pri tome ako je indeks 1 stampa slovo vrste
;; indeks je promenljiva koja uzima vrednosti 0, 1 i 2 i koristi se za numerizaciju redova vrste
;; Svaki red vrste se prosledjuje funkciji stampajPolje
(defun --stampajVrstu (vrsta indeks indeksVrste)
(cond
    ((null vrsta) " ")
    (t(progn 
            (if (eq (mod indeks 3) 1) (format t "~a " (vratiSlovoIndeksaVrste indeksVrste)) (format t "  "))
            (--stampajPolje (car vrsta))
            (--stampajVrstu (cdr vrsta) (+ indeks 1) indeksVrste)))
))

;; Polje sadrzi 1 red vrste
(defun --stampajPolje (polje)
(progn  
    (mapcar '--stampajVrednostPolja polje) 
    (format t "~%")))

(defun --stampajVrednostPolja (redPolja)
(cond
    ((eq nil (car redPolja)) (format t "       "))
    (t (format t " ~a ~a ~a " (car redPolja) (cadr redPolja) (caddr redPolja)))
))

(defun transformisiTabluZaStampanje (tabla)
(mapcar '--transformisiVrstuZaStampanje tabla))

(defun --transformisiVrstuZaStampanje (vrsta)
(append 
    (list (mapcar (lambda (polje) (nth 0 polje)) vrsta)) 
    (append 
        (list (mapcar (lambda (polje) (nth 1 polje)) vrsta)) 
        (list (mapcar (lambda (polje) (nth 2 polje)) vrsta)))))

(defun normalizujTabluZaStampanje (tabla)
(mapcar '--normalizujVrstuZaStampanje tabla))

(defun --normalizujVrstuZaStampanje (vrsta)
(mapcar '--razbijNaTri vrsta))

(defun --razbijNaTri (polje)
(cond 
    ((eq polje nil) '(nil nil nil))
    ((null (cdddr polje)) (list polje))
    (t (append (list (append (append (list (car polje)) (list (cadr polje))) (list (caddr polje)))) (--razbijNaTri (cdddr polje))))
))

(defun vratiSlovoIndeksaVrste (broj)
(code-char (+ (char-code #\A)  broj)))

;ova funkcija je dodata jer vratiSLovoIndeksaVrste vraca #\C npr i to je zgogno samo kod stampanja
(defun vratiSlovoVrste (broj)
(case broj
    (1 'A)
    (2 'B)
    (3 'C)
    (4 'D)
    (5 'E)
    (6 'F)
    (7 'G)
    (8 'H)
    (9 'I)
    (10 'J)
    (11 'K)
    (12 'L)
    (13 'M)
    (14 'N)
    (15 'O)
    (16 'P)
    (17 'Q)
    (18 'R)
    (19 'S)
    (20 'T)
    (21 'U)
    (22 'V)
    (23 'W)
    (24 'X)
    (25 'Y)
    (26 'Z) 
)
)

(defun vratiBrojIndeksaVrste (slovo)
(case slovo
    (A '1)
    (B '2)
    (C '3)
    ('D '4)
    (E '5)
    (F '6)
    (G '7)
    (H '8)
    (I '9)
    (J '10)
    (K '11)
    (L '12)
    (M '13)
    (N '14)
    (O '15)
    (P '16)
    (Q '17)
    (R '18)
    (S '19)
    ('T '20)
    (U '21)
    (V '22)
    (W '23)
    (X '24)
    (Y '25)
    (Z '26)
    (t '0)
))

(defun vratiIndeksKolone (key)
(case key
    (1 '1)
    (5 '2)
    (9 '3)
    (13 '4)
    (17 '5)
    (21 '6)
    (25 '7)
    (29 '8)
    (33 '9)
    (37 '10)
    (41 '11)
    (45 '12)
    (49 '13)
    (53 '14)
    (57 '15)
    (61 '16)
    (65 '17)
    (69 '18)
    (73 '19)
    (77 '20)
    (81 '21)
    (85 '22)
    (89 '23)
    (93 '24)
    (97 '25)
))

(defun stampajZaglavljeZaKolone (lista1 lista2)
(cond
    ((null lista1) " ")
    ((eq (car lista1) (car lista2)) (progn (format t "  ~a " (vratiIndeksKolone (car lista1))) (stampajZaglavljeZaKolone (cdr lista1) (cdr lista2))))
    (t (progn (format t " ") (stampajZaglavljeZaKolone lista1 (cdr lista2))))
))

;; Validacione funkcije



(defun validacijaDimenzija (dimenzija)
(cond
    ((and (eq (mod (izracunajBrojFigura dimenzija) 8) 0) (>= dimenzija 8)) T)
    (t NIL)
))


(defun validacijaZapisaPoteza (potez)
(cond
    ((not (listp potez)) NIL) 
    ((or (< (length potez) 2) (> (length potez) 3)) NIL)
    ( (not (and (listp (car potez)) (listp (cadr potez)))) NIL) 
    ((or (not (eq (length (car potez)) 2)) (not (eq (length (cadr potez)) 2))) NIL) 
    (t T)
))

(defun uGranicamaTable (indeks dimenzija)
(cond ((null indeks) NIL) 
(t(if (and (not(> indeks dimenzija)) (> indeks 0)) t NIL))
))


(defun daLiJePoljeCrno (vrsta kolona)
(= (mod (+ vrsta kolona) 2) 0)
)

(defun validacijaIndeksa (zapisPolja tabla)
(and (uGranicamaTable (vratiBrojIndeksaVrste (car zapisPolja)) (length tabla)) (uGranicamaTable (cadr zapisPolja) (length tabla)) 
(daLiJePoljeCrno (vratiBrojIndeksaVrste (car zapisPolja)) (cadr zapisPolja))
) 
)

(defun postojiIgracNaPolju (zapisPolja tabla igrac) 
(let ((POLJE (vratiPoljeMatrice (vratiBrojIndeksaVrste (car zapisPolja)) (cadr zapisPolja) tabla) ))
(and (< 0 (length (remove '- POLJE))) (member igrac POLJE ) )
)
)

(defun daLiJePoljePrazno (polje tabla)
(let ((POLJE (vratiPoljeMatrice (car polje) (cadr polje) tabla) ))
(and (zerop (length (remove '- POLJE))) (not (null POLJE)))
)
)

(defun validacijaPoljaUPotezu (potez tabla igrac) 
(if   (and (validacijaIndeksa (car potez) tabla) (validacijaIndeksa (cadr potez) tabla) (postojiIgracNaPolju (car potez) tabla igrac))
            (let ((VALIDNIPOTEZI (vratiValidnePoteze tabla (vratiBrojIndeksaVrste (caar potez)) (cadar potez))))
                (if VALIDNIPOTEZI (poljeNaDijagonali potez tabla VALIDNIPOTEZI) '-1  )
            )

            '()

))

(defun proveriDaLiJeDeoPuta  (polje put)
(cond ((null put) '())
      ((equalp polje (car put)) T)
      (t (proveriDaLiJeDeoPuta polje (cdr put)))))
      

(defun poljeNaDijagonali (potez tabla VALIDNIPOTEZI)
(let ((PUTDOPOLJA (append (list (konvertujIndeksPolja (cadr potez ))) (list '1))) )
      (if(proveriDaLiJeDeoPuta PUTDOPOLJA VALIDNIPOTEZI) T '())
))


(defun nadjiNajnizuPlocicuIgraca (pozicija polje igrac) 
(cond ((null polje) '())
((equalp (car polje) igrac) pozicija)
(t(nadjiNajnizuPlocicuIgraca (1+ pozicija) (cdr polje) igrac))
)
)

(defun proveriPlocicu (pozicija polje igrac)
(equalp igrac (nth pozicija (reverse polje))) 
)


(defun noviPotez (stariPotez novaVisina)
(list (car stariPotez) (cadr stariPotez) novaVisina)
)
 
(defun proveriVelicineStekova (potez POLJE DRUGOPOLJE)
(cond ((and (zerop (caddr potez)) (zerop (length (remove '- DRUGOPOLJE)))) potez)
      ((or (>  (1+ (caddr potez)) (length (remove '- DRUGOPOLJE))) (and (not (zerop (caddr potez))) (zerop (length (remove '- DRUGOPOLJE))))) NIL)
      ((> (+ (- (length (remove '- POLJE)) (caddr potez))  (length (remove '- DRUGOPOLJE))) 8) NIL)
      (t potez)
)
)

(defun ispravanPotez (potez igrac tabla)
(let ((POLJE (vratiPoljeMatrice (vratiBrojIndeksaVrste (caar potez )) (cadar potez) tabla))
      (DRUGOPOLJE (vratiPoljeMatrice (vratiBrojIndeksaVrste (caadr potez)) (cadadr potez) tabla)))

(cond ((= (length potez) 2) (proveriVelicineStekova (noviPotez potez (nadjiNajnizuPlocicuIgraca 0 (reverse POLJE) igrac)) POLJE DRUGOPOLJE)) 
      ((or 
        (< (caddr potez) 0) 
        (> (caddr potez) (length (remove '- POLJE))) 
        (not (proveriPlocicu (caddr potez) POLJE igrac ))
       ) '())
      ((eq (proveriVelicineStekova potez POLJE DRUGOPOLJE) nil) '())
      (:else potez)
)))

;nalazi sve visine u okviru steka na kojima je plocica igraca
(defun nadjiSvePlociceIgraca (igrac polje pozicija)
(cond ((null polje) '())
      ((equalp (car polje) igrac) (append (list pozicija) (nadjiSvePlociceIgraca igrac (cdr polje) (1+ pozicija))))
      (t (nadjiSvePlociceIgraca igrac (cdr polje) (1+ pozicija)))
))



;ako mi je polje prazno, onda mogu samo ceo stek da prebacim, pa proverim da li je na visini nula moj igrac, inace moram da proverim za sve visine
(defun napraviSveIspravnePoteze (polje odredista igrac tabla)
(cond ((null odredista) '())
      ;ako na odredistu nema igraca, mogu da se pomerim samo sa nulte visine
      ( (and
            (daLiJePoljePrazno (caar odredista) tabla) 
            (proveriPlocicu 0 (vratiPoljeMatrice (car polje) (cadr polje) tabla) igrac )
        ) 
        (append (list (list (list (vratiSlovoVrste (car polje)) (cadr polje)) 
                                  (list (vratiSlovoVrste (caaar odredista)) (cadaar odredista)) 0) ) 
                (napraviSveIspravnePoteze polje (cdr odredista) igrac tabla))
      )
      ;ako je polje prazno, a moja plocica nije nulta, pravi naredni potez, ovo preskoci!
      ((daLiJePoljePrazno (caar odredista) tabla) (napraviSveIspravnePoteze polje (cdr odredista) igrac tabla))
      (t (append 
           (napraviPotezeDoOdredista polje (nadjiSvePlociceIgraca igrac (reverse (vratiPoljeMatrice (car polje) (cadr polje) tabla)) 0) 
                                     (caar odredista) igrac tabla)
           (napraviSveIspravnePoteze polje (cdr odredista) igrac tabla)
         )
      )
)
)

;ako je ispravan potez, dodati ga u listu i nastaviti rekurzivno
;inace idemo dalje
(defun napraviPotezeDoOdredista ( polje visine odrediste igrac tabla)
(cond ((null visine) '())
      ( (ispravanPotez (list (list (vratiSlovoVrste (car polje)) (cadr polje)) (list (vratiSlovoVrste (car odrediste)) (cadr odrediste)) (car visine)) igrac tabla ) 
        (append 
            (list (list (list (vratiSlovoVrste (car polje)) (cadr polje)) (list (vratiSlovoVrste (car odrediste)) (cadr odrediste)) 
            (car visine)) )

            (napraviPotezeDoOdredista polje (cdr visine) odrediste igrac tabla))
      )
      (t (napraviPotezeDoOdredista polje (cdr visine) odrediste igrac tabla))
)
)

;kao argument dobija listu svih polja sa kojih igrac moze da igra i za svako od tih polja izgenerise poteze
(defun nadjiPotezeZaSvaPopunjenaPolja (polja igrac tabla)
(cond ((null polja) '())
      (t (append 
            (napraviSveIspravnePoteze (car polja) (vratiValidnePoteze tabla (caar polja) (cadar polja)) igrac tabla )
            (nadjiPotezeZaSvaPopunjenaPolja (cdr polja) igrac tabla)
         )
      )
)
)

;za svaki moguci potez izgenerise novu tablu
(defun izgenerisiSvaStanja (tabla igrac potezi) 
(cond ((null potezi) '())
      (t (append 
            (list (odigraj tabla (konvertujpotez (car potezi)) (prenos tabla (konvertujpotez (car potezi)))))
            (izgenerisiSvaStanja tabla igrac (cdr potezi))
         )
      )
)
)

(defun odigrajSvePoteze (tabla igrac)
   (izgenerisiSvaStanja tabla igrac 
                        (nadjiPotezeZaSvaPopunjenaPolja (listaKorisnikovihPolja tabla igrac (nadjiPolja (length tabla) 1)) igrac tabla)
   )
)

(defun vratiSvePotezeSaTable (tabla igrac)
(nadjiPotezeZaSvaPopunjenaPolja (listaKorisnikovihPolja tabla igrac (nadjiPolja (length tabla) 1)) igrac tabla))

;;; Glavni fajl za igru



(defun igra (tabla skor igrac trenutniIgrac)
(progn
    (stampajTablu tabla)
    (stampanjePoruke skor)
    (cond ((eq (div (length tabla) 3) (car skor)) (stampanjePoruke "Pobedio je igrac X."))
          ((eq (div (length tabla) 3) (cadr skor)) (stampanjePoruke "Pobedio je igrac O."))
    (t 
        (progn 
            (format t "~%Trenutno igra igrac ~a.~%" trenutniIgrac)
            (if (eq trenutniIgrac igrac)
                ;true
                (igraKorisnik tabla skor igrac trenutniIgrac)
                ;else
                (igraRacunar tabla skor igrac trenutniIgrac)
            )
        )
    )
)))

(defun igraRacunar (tabla skor igrac trenutniIgrac)
(let 
    ((PRAVIPOTEZRACUNARA  (cadr (minmaxAB tabla 4 -999999 999999 trenutniIgrac trenutniIgrac))))
    (if (null PRAVIPOTEZRACUNARA)
    (progn (stampanjePoruke "Racunar nema validan potez!") (igra tabla skor (vratiSledecegIgraca igrac) (vratiSledecegIgraca igrac)))
    (progn 
        (stampanjePoruke "Racunar igra potez: ")
        (stampajInlinePoruku PRAVIPOTEZRACUNARA)
        (stampanjePoruke " ")
        (let  ((NOVATABLA2 (odigraj tabla (konvertujpotez PRAVIPOTEZRACUNARA) (prenos tabla (konvertujpotez PRAVIPOTEZRACUNARA)))))
            (igra 
                (skloni NOVATABLA2 (konvertujIndeksPolja (cadr PRAVIPOTEZRACUNARA))) 
                (promeniSkor NOVATABLA2 skor (konvertujIndeksPolja (cadr PRAVIPOTEZRACUNARA))) 
                igrac
                (vratiSledecegIgraca trenutniIgrac)
            ) 
        )
    )
    )
))


(defun igraKorisnik (tabla skor igrac trenutniIgrac)
(let ((POTEZ (unosKorisnika "Unesite potez: (potez je oblika ((E 3) (F 4) 0)):")))
    (if (validacijaZapisaPoteza POTEZ) 
        ;; true
        (let ((VALIDACIJAPOLJA (validacijaPoljaUPotezu potez tabla igrac)))
            (cond  
                ((eq (vratiSvePotezeSaTable tabla igrac) NIL) (progn (stampanjePoruke "Ne postoje validni potezi.") (igra tabla skor (vratiSledecegIgraca igrac) (vratiSledecegIgraca trenutniIgrac))))
                ((eq VALIDACIJAPOLJA '-1) (progn (stampanjePoruke "Ne postoje validni potezi.") (igra tabla skor (vratiSledecegIgraca igrac) (vratiSledecegIgraca trenutniIgrac))))
                ((eq VALIDACIJAPOLJA NIL) (progn (stampanjePoruke "Nevalidan potez.") (igra tabla skor igrac trenutniIgrac)))
                (t  (let ((PRAVIPOTEZ (ispravanPotez POTEZ igrac tabla)))
                        (if (null PRAVIPOTEZ)
                            (progn (stampanjePoruke "Nevalidna visina!") (igra tabla skor igrac trenutniIgrac))
                            (progn (let ((NOVATABLA (odigraj tabla (konvertujpotez PRAVIPOTEZ) (prenos tabla (konvertujpotez PRAVIPOTEZ)))))
                                        (igra 
                                            (skloni NOVATABLA (konvertujIndeksPolja (cadr PRAVIPOTEZ))) 
                                            (promeniSkor NOVATABLA skor (konvertujIndeksPolja (cadr PRAVIPOTEZ))) 
                                            igrac
                                            (vratiSledecegIgraca trenutniIgrac)) 
                                    )
                            )
                        )
                    )
                )
            )
        )
        ;; else
        (progn (stampanjePoruke "Nevalidan zapis poteza.") (igra tabla skor igrac trenutniIgrac))
    )
))

(defun main ()
(let ((DIMENZIJA (unosKorisnika "Unesite dimenzije tabele (8 ili 10):")))
(cond
    ((eq (validacijaDimenzija DIMENZIJA) T) 
    (progn 
        (let ((KORISNOKOVIGRAC (unosKorisnika "Izaberite svog igraca (X igrac igra prvi, O igrac igra drugi):")))
             (if (or (eq KORISNOKOVIGRAC 'X) (eq KORISNOKOVIGRAC 'O)) 
                (igra (napraviTablu DIMENZIJA DIMENZIJA) '(0 0) KORISNOKOVIGRAC 'X) 
            (progn (stampanjePoruke "Nevalidan unos igraca.") (main)))
        )))
    (t (progn (stampanjePoruke "Dimenzije nevalidne.") (main)))
)
))

(main)