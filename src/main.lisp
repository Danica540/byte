(require "./game/game.lisp")
(require "./helpers/helpers.lisp")

(defun main ()
(let ((DIMENZIJA (unosKorisnika "Unesite dimenzije tabele (8 ili 10):")))
(cond
    ((eq (validacijaDimenzija DIMENZIJA) T) 
    (progn 
        (let ((KORISNOKOVIGRAC (unosKorisnika "Izaberite svog igraca (X igrac igra prvi, O igrac igra drugi):")))
             (if (or (eq KORISNOKOVIGRAC 'X) (eq KORISNOKOVIGRAC 'O)) 
                (igra (napraviTablu DIMENZIJA DIMENZIJA) '(0 0) KORISNOKOVIGRAC 'X) 
            (progn (stampanjePoruke "Nevalidan unos igraca.") (main)))
        )))
    (t (progn (stampanjePoruke "Dimenzije nevalidne.") (main)))
)
))

(main)