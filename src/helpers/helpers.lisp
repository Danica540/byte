;; Pomocne funkcije za stampanje u unos poruka iz terminala

(defun stampanjePoruke (poruka)
(format t "~%~a~%" poruka))

(defun stampajInlinePoruku (poruka)
(format t "~a" poruka))

(defun unosKorisnika (poruka)
    (format t "~%~a~%" poruka)
    (read)
)

;; aritmeticka progresija
(defun progres (n k)
(cond
    ((> n 0) (append (progres (- n 1) k) (list (+ 1 (* k (- n 1))))))
))

;; generise listu sa elementima od 0 do brojElemenata
(defun generisiListu (brojElemenata)
(cond 
    ((eq brojElemenata 0) '())
    (t (append (list (- brojElemenata 1)) (generisiListu (- brojElemenata 1))))
))