;;; Glavni fajl za igru
(require "./game-display.lisp")
(require "./game-table.lisp")
(require "./game-win-checks.lisp")
(require "./game-validations.lisp")
(require "./game-turns.lisp")
(require "./game-moves.lisp")


(defun igra (tabla skor igrac trenutniIgrac)
(progn
    (stampajTablu tabla)
    (stampanjePoruke skor)
    (cond ((eq (div (length tabla) 3) (car skor)) (stampanjePoruke "Pobedio je igrac X."))
          ((eq (div (length tabla) 3) (cadr skor)) (stampanjePoruke "Pobedio je igrac O."))
    (t 
        (progn 
            (format t "~%Trenutno igra igrac ~a.~%" trenutniIgrac)
            (if (eq trenutniIgrac igrac)
                ;true
                (igraKorisnik tabla skor igrac trenutniIgrac)
                ;else
                (igraRacunar tabla skor igrac trenutniIgrac)
            )
        )
    )
)))

(defun igraRacunar (tabla skor igrac trenutniIgrac)
(let 
    ((PRAVIPOTEZRACUNARA  (cadr (minmaxAB tabla 4 -999999 999999 trenutniIgrac trenutniIgrac))))
    (if (null PRAVIPOTEZRACUNARA)
    (progn (stampanjePoruke "Racunar nema validan potez!") (igra tabla skor igrac (vratiSledecegIgraca igrac)))
    (progn 
        (stampanjePoruke "Racunar igra potez: ")
        (stampajInlinePoruku PRAVIPOTEZRACUNARA)
        (stampanjePoruke " ")
        (let  ((NOVATABLA2 (odigraj tabla (konvertujpotez PRAVIPOTEZRACUNARA) (prenos tabla (konvertujpotez PRAVIPOTEZRACUNARA)))))
            (igra 
                (skloni NOVATABLA2 (konvertujIndeksPolja (cadr PRAVIPOTEZRACUNARA))) 
                (promeniSkor NOVATABLA2 skor (konvertujIndeksPolja (cadr PRAVIPOTEZRACUNARA))) 
                igrac
                (vratiSledecegIgraca trenutniIgrac)
            ) 
        )
    )
    )
))


(defun igraKorisnik (tabla skor igrac trenutniIgrac)
(let ((POTEZ (unosKorisnika "Unesite potez: (potez je oblika ((E 3) (F 4) 0)):")))
    (if (validacijaZapisaPoteza POTEZ) 
        ;; true
        (let ((VALIDACIJAPOLJA (validacijaPoljaUPotezu potez tabla igrac)))
            (cond  
                ((eq (vratiSvePotezeSaTable tabla igrac) NIL) (progn (stampanjePoruke "Ne postoje validni potezi.") (igra tabla skor igrac (vratiSledecegIgraca trenutniIgrac))))
                ((eq VALIDACIJAPOLJA '-1) (progn (stampanjePoruke "Ne postoje validni potezi.") (igra tabla skor (vratiSledecegIgraca igrac) (vratiSledecegIgraca trenutniIgrac))))
                ((eq VALIDACIJAPOLJA NIL) (progn (stampanjePoruke "Nevalidan potez.") (igra tabla skor igrac trenutniIgrac)))
                (t  (let ((PRAVIPOTEZ (ispravanPotez POTEZ igrac tabla)))
                        (if (null PRAVIPOTEZ)
                            (progn (stampanjePoruke "Nevalidna visina!") (igra tabla skor igrac trenutniIgrac))
                            (progn (let ((NOVATABLA (odigraj tabla (konvertujpotez PRAVIPOTEZ) (prenos tabla (konvertujpotez PRAVIPOTEZ)))))
                                        (igra 
                                            (skloni NOVATABLA (konvertujIndeksPolja (cadr PRAVIPOTEZ))) 
                                            (promeniSkor NOVATABLA skor (konvertujIndeksPolja (cadr PRAVIPOTEZ))) 
                                            igrac
                                            (vratiSledecegIgraca trenutniIgrac)) 
                                    )
                            )
                        )
                    )
                )
            )
        )
        ;; else
        (progn (stampanjePoruke "Nevalidan zapis poteza.") (igra tabla skor igrac trenutniIgrac))
    )
))

