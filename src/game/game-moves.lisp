
(require "./game/inference-engine.lisp")
(require "./game/game-win-checks.lisp")


;;minmax sa alfa beta odsecanjem
;;(nadjiPotezeZaSvaPopunjenaPolja (listaKorisnikovihPolja tabla igrac (nadjiPolja (length tabla) 1)) igrac tabla)
(defun minmaxAB (tabla dubina a b igrac originalIgrac)
    (let 
        ((listaPoteza (vratiSvePotezeSaTable tabla igrac))
        (bestvalue '()) (najboljiPotez '()) (state '())
        (tmp '()) (alpha a) (beta b))
        (cond 
            ((or (null listaPoteza) (= dubina 0)) (setf bestvalue (evaluirajStanje tabla originalIgrac)))
            ((equal igrac 'X) ;X plays the move - Maximizing igrac
                (progn
                    (setf bestvalue -999999)
                    (loop for potez in listaPoteza do
                        (progn 
                            (setf state (odigraj tabla (konvertujpotez potez) (prenos tabla (konvertujpotez potez))))
                            (setf tmp (minmaxAB state (- dubina 1) alpha beta 'O originalIgrac))
                            (if (> (car tmp) bestvalue) (progn (setf bestvalue (car tmp)) (setf najboljiPotez potez)))
                            (setf alpha (max alpha bestvalue))
                            (if (>= alpha beta) (return-from minmaxAB (list bestvalue najboljiPotez))))
                    )
                )
            )
            ((equal igrac 'O) ;O plays the move - Minimizing igrac
                (progn
                    (setf bestvalue 999999)
                    (loop for potez in listaPoteza do
                        (progn
                            (setf state (odigraj tabla (konvertujpotez potez) (prenos tabla (konvertujpotez potez))))
                            (setf tmp (minmaxAB state (- dubina 1) alpha beta 'X originalIgrac))
                            (if (< (car tmp) bestvalue) (progn (setf bestvalue (car tmp)) (setf najboljiPotez potez)))
                            (setf beta (min beta bestvalue))
                            (if (>= alpha beta) (return-from minmaxAB (list bestvalue najboljiPotez))))))
            )) 
            (list bestvalue najboljiPotez)
    )
)

;; ============================== EVALUACIJA ===============================

;; equal
(defun !eq (x y)
    (eq x y))

;; not equal
(defun !ne (x y)
    (not (eq x y)))

(defun evaluirajStanje (tabla igrac)
    (prepare-knowledge (odrediPravila) (odrediCinjenice tabla igrac) 10) 
    (cond 
    ((eq igrac 'X) (+  
        (cadaar (infer '(Evaluate ?value 'move 'X)))
        (cadaar (infer '(Evaluate ?value 'victory 'X)))
        (cadaar (infer '(Evaluate ?value 'neighbour 'X)))))
((eq igrac 'O) (+  
        (cadaar (infer '(Evaluate ?value 'move 'O)))
        (cadaar (infer '(Evaluate ?value 'victory 'O)))
        (cadaar (infer '(Evaluate ?value 'neighbour 'O)))
))
    )
    )

(defun odrediPravila ()
    '(
            (if (Ready-for-evaluation ?value ?param ?igrac)
                then (Evaluate ?value ?param ?igrac))

            (if (and (Check-neighbours ?value) (!eq ?param 'neighbour))
                then (Ready-for-evaluation ?value ?param ?igrac))
            
            (if (and (Figure-difference ?value) (!eq ?param 'move))
                then (Ready-for-evaluation ?value ?param ?igrac))

            (if (and (Stack-X ?value) (!eq ?param 'victory) (!eq ?igrac 'X) (!ne ?value nil))
                then (Ready-for-evaluation +10000 ?param ?igrac))

            (if (and (Stack-X ?value) (!eq ?param 'victory) (!eq ?igrac 'X) (!eq ?value nil))
                then (Ready-for-evaluation 0 ?param ?igrac))

            (if (and (Stack-O ?value) (!eq ?param 'victory) (!eq ?igrac 'O) (!ne ?value nil))
                then (Ready-for-evaluation -10000 ?param ?igrac))

            (if (and (Stack-O ?value) (!eq ?param 'victory) (!eq ?igrac 'O) (!eq ?value nil))
                then (Ready-for-evaluation 0 ?param ?igrac))
           ))

(defun odrediCinjenice (tabla igrac)
  (list
        (list 'Check-neighbours (proveriSusede tabla igrac))
        (list 'Figure-difference (izracunajRazlikuBrojaFigura tabla 13))
        (list 'Stack-X (proveriDaLiPostojiStekIgraca tabla 'X))
        (list 'Stack-O (proveriDaLiPostojiStekIgraca tabla 'O))
))

(defun izracunajRazlikuBrojaFigura (tabla faktor)
    (* (- (izracunajBrojVrsnihFigura tabla 'X) (izracunajBrojVrsnihFigura tabla 'O)) faktor (izracunajFaktor tabla)))

(defun izracunajFaktor (tabla)
(div (* (length tabla) 4) (brojStekova tabla))
)

;;provera borj figura igraca na vrhu
(defun izracunajBrojVrsnihFigura (tabla igrac)
(cond 
    ((null tabla) 0)
    (t (+ (brojVrsnihFiguraUVrsti (car tabla) igrac) (izracunajBrojVrsnihFigura (cdr tabla) igrac ) ))
)
)

(defun brojVrsnihFiguraUVrsti (vrsta igrac)
(cond 
    ((null vrsta) 0)
    (t (+ (proveriFiguruNaVrhu (car vrsta) igrac) (brojVrsnihFiguraUVrsti (cdr vrsta) igrac) ))
)
)

(defun proveriFiguruNaVrhu (polje igrac)
(cond
    ((eq (car (remove '- polje)) igrac) 1)
    (t 0)
)
)

;; provera suseda
(defun proveriSusede (tabla igrac)
     (+ 
            (vratiEvaluacijuSuseda (odigrajSvePoteze tabla (vratiSledecegIgraca igrac)) igrac)
            (proveriTrenutnoStanje tabla igrac)
          )
)

(defun brojStekova (tabla)
(cond 
    ((null tabla) 0)
    (t (+ (brojStekovaUVrsti (car tabla)) (brojStekova (cdr tabla))))
)
)

(defun brojStekovaUVrsti (vrsta)
(cond 
    ((null vrsta) 0)
    (t (+ (stekNaPolju (car vrsta))  (brojStekovaUVrsti (cdr vrsta))) )
)
)

(defun stekNaPolju (polje)
(cond 
    ((> (length (remove '- polje)) 0) 1)
    (t 0)
)
)

(defun proveriTrenutnoStanje (tabla igrac)
(cond
    ((and (proveriDaLiPostojiStekIgraca tabla igrac) (eq igrac 'X)) 300)
    ((and (proveriDaLiPostojiStekIgraca tabla (vratiSledecegIgraca igrac)) (eq igrac 'X)) -300)
    ((and (proveriDaLiPostojiStekIgraca tabla igrac) (eq igrac 'O)) -300)
    ((and (proveriDaLiPostojiStekIgraca tabla (vratiSledecegIgraca igrac)) (eq igrac 'O)) 300)   
    (t 0)
)
)

(defun vratiEvaluacijuSuseda (sveTable igrac)
(cond 
    ((null sveTable) 0)
    ((and (proveriDaLiPostojiStekIgraca (car sveTable) igrac) (eq igrac 'X)) (+ 100 (vratiEvaluacijuSuseda (cdr sveTable) igrac)))
    ((and (proveriDaLiPostojiStekIgraca (car sveTable) igrac) (eq igrac 'O)) (- 100 (vratiEvaluacijuSuseda (cdr sveTable) igrac)))
    ((and (proveriDaLiPostojiStekIgraca (car sveTable) (vratiSledecegIgraca igrac)) (eq (vratiSledecegIgraca igrac) 'X)) (+ (vratiEvaluacijuSuseda (cdr sveTable) igrac) 100))
    ((and (proveriDaLiPostojiStekIgraca (car sveTable) (vratiSledecegIgraca igrac)) (eq (vratiSledecegIgraca igrac) 'O)) (- (vratiEvaluacijuSuseda (cdr sveTable) igrac) 100))
    (t (+ 0 (vratiEvaluacijuSuseda (cdr sveTable) igrac)))
))

;; proverava za svako polje da li se nalazi stek na njemu i koja se vrsna figura
(defun proveriDaLiPostojiStekIgraca(tabla igrac)
(cond
    ((null tabla) NIL)
    ((eq (proveriStekUVrsti (car tabla) igrac) T) T)
    (t (proveriDaLiPostojiStekIgraca (cdr tabla) igrac))
))

(defun proveriStekUVrsti ( vrsta igrac)
(cond 
    ((null vrsta) NIL)
    ((eq (postojiStek (car vrsta) igrac) T) T)
    (t (proveriStekUVrsti (cdr vrsta) igrac))
))

(defun postojiStek (polje igrac)
(cond 
    ((and (eq (length (remove '- polje)) 8) (eq (nth 1 polje) igrac)) T)
    (t NIL)
))

;------------------------- ZAVRSEN DEO SA EVALUACIJOM -----------------------------------------


;;(stampanjePoruke (transformisiNajkracePuteve (vratiValidnePotezeNaOsnovuNajkracegRastojanja '() (vratiPoljeMatrice 1 3 *tablaZaPobedu*) 1 3 0 *tablaZaPobedu*)))
(defun vratiValidnePoteze (tabla indeksVrste indeksKolone)
(obrisiDuplikate (transformisiNajkracePuteve (vratiValidnePotezeNaOsnovuNajkracegRastojanja '() (vratiPoljeMatrice indeksVrste indeksKolone tabla) indeksVrste indeksKolone 0 tabla))))

;; Funkcija za nalazenje validnih poteza na osnovu najkraceg puta do igrac
(defun vratiValidnePotezeNaOsnovuNajkracegRastojanja (trenutnaPutanja trenutnoPolje indeksVrste indeksKolone metrika tabla)
(cond
    ((eq (daLiJePoljeObidjeno trenutnaPutanja indeksVrste indeksKolone) T) '()) ;; uslov za izlazak je da ne obilazimo 2 puta isto polje
    ((or (<= indeksKolone 0) (> indeksKolone (length tabla))) '()) ;; ako polje prelazi granice tabla izadji
    ((or (<= indeksVrste 0) (> indeksVrste (length tabla))) '()) ;; ako polje prelazi granice tabla izadji
    ((> metrika (length tabla)) '())
    ((and 
        (> metrika 0) ;; nije prvi prolaz
        (eq (daLiJeStekPopunjen trenutnoPolje) T)
    ) '())
    ((and 
        (> metrika 0) ;; nije prvi prolaz
        (and 
            (eq (daLiPoljeSadrziFiguru trenutnoPolje) T) 
            (eq (daLiJeStekPopunjen trenutnoPolje) NIL)
        )
    )
    (list (reverse (dodajUListu (list (append (list (append (list indeksVrste) (list indeksKolone))) (list metrika ))) trenutnaPutanja)))) ;; ako naidjes na polje gde mozes da prebacis figuru vrati taj put
    (t (let ((PUTANJA (dodajUListu (list (append (list (append (list indeksVrste) (list indeksKolone))) (list metrika ))) trenutnaPutanja)))
        (append 
            (append 
                (vratiValidnePotezeNaOsnovuNajkracegRastojanja 
                    PUTANJA
                    (vratiPoljeMatrice (- indeksVrste 1) (- indeksKolone 1) tabla) 
                    (- indeksVrste 1) 
                    (- indeksKolone 1) 
                    (+ metrika 1)
                    tabla)
                (vratiValidnePotezeNaOsnovuNajkracegRastojanja 
                    PUTANJA
                    (vratiPoljeMatrice (- indeksVrste 1) (+ indeksKolone 1) tabla) 
                    (- indeksVrste 1) 
                    (+ indeksKolone 1) 
                    (+ metrika 1)
                    tabla)
            )
            (append 
                (vratiValidnePotezeNaOsnovuNajkracegRastojanja 
                    PUTANJA
                    (vratiPoljeMatrice (+ indeksVrste 1) (- indeksKolone 1) tabla) 
                    (+ indeksVrste 1) 
                    (- indeksKolone 1) 
                    (+ metrika 1)
                    tabla)
                (vratiValidnePotezeNaOsnovuNajkracegRastojanja 
                    PUTANJA
                    (vratiPoljeMatrice (+ indeksVrste 1) (+ indeksKolone 1) tabla) 
                    (+ indeksVrste 1) 
                    (+ indeksKolone 1) 
                    (+ metrika 1)
                    tabla)
            )
        )
    ))
))

;; Proverava da li je polje sa indeksom vrste i indeksom kolone vec obidjeno, tj da li se vec nalazi u prosledjenoj listi
;; trenutna putanja je oblika: ( ((1 2) 4) ((1 1) 1) ((2 2) 1) ((1 3) 2) )
(defun daLiJePoljeObidjeno (trenutnaPutanja indeksVrste indeksKolone)
(cond
      ((null trenutnaPutanja) NIL)
      ((equal (caar trenutnaPutanja) (append (list indeksVrste) (list indeksKolone))) T)
      (t (daLiJePoljeObidjeno (cdr trenutnaPutanja) indeksVrste indeksKolone))
))

;; ako je polje vec obidjeno ne dodaje u listu, ako polje nije obidjeno vraca listu sa elementom
(defun dodajUListu (element lista) 
(cond
    ((equal (daLiJePoljeObidjeno lista (caar element) (cadr element)) T) lista)
    (t (append element lista))
))

;; Funkcija koja proverava da li je polje prazno ili sadrzi makar jednu figuru
(defun daLiPoljeSadrziFiguru (trenutnoPolje)
(cond
    ((eq (length (remove '- trenutnoPolje)) 0) NIL)
    (t T)
))

;; Funkcija koja izdvaja polja koja imaju metriku 1 i validna su
(defun transformisiNajkracePuteve (lista) 
(vratiNajblizePuteve lista (nadjiMinimumDuzinePuta lista)))

(defun nadjiMinimumDuzinePuta (lista)
(cond
    ((null (cdr lista)) (length (car lista)))
    (t (min (length (car lista)) (nadjiMinimumDuzinePuta (cdr lista))))
))

(defun vratiNajblizePuteve (lista min)
(cond
    ((null lista) '())
    ((eq (length (car lista)) min) (append (list (cadar lista)) (vratiNajblizePuteve (cdr lista) min)))
    (t (append '() (vratiNajblizePuteve (cdr lista) min)))
))

(defun obrisiDuplikate (listaPuteva)
(cond ((null listaPuteva) '())
      (t (remove-duplicates listaPuteva :test 'equal :from-end t))
)
)

;;--------- funkcije za pomeranje figura na tabli

(defun konvertujIndeksPolja (polje)
(list ( vratiBrojIndeksaVrste (car polje)) (cadr polje)))

;;(konvertujIndeksPolja polje)
;;Prima jedan parametar oblika *(A 4)*, polje na tabli
;;vraca konvertovanu oznaku polja za igru u obliku *(1 4)* 

(defun konvertujPotez (praviPotez)
(append (list (konvertujIndeksPolja (car praviPotez)) (konvertujIndeksPolja (cadr praviPotez))) (last praviPotez)))

;;(konvertujPotez praviPotez)
;;prima jedan argument tipa praviPotez *((A 3)(B 2) 5)*
;;vraca potez sa promenjenim indeksim *((1 3)(2 2) 5)*

(defun prenos (tabla potez)
(listaZaPrenos (remove '- (vratiPoljeMatrice (caar potez) (cadar potez) tabla)) (caddr potez)))

;;(prenos tabla potez)
;; dva argumenta tabla i potez u konvertovano obliku
;;vraca prenos za zadati potez u obliku liste *(x o x)*

(defun listaZaPrenos (stek visina)
(cond ((<= (length stek) visina) '())
      (t (cons (car stek) (listaZaPrenos (cdr stek) visina)))
))

;;(listaZaPrenos stek visina)
;;prima dva argumenta *(x o x o x x) 2* prvi je stek sa koga se uzima drugi je visina sa koje se uzima (krece od 0)
;;vraca listu karaktera koji se prenose *(x o x o)*


(defun smanjiRedUPotezu (potez)
(list (list (- (caar potez) 1) (cadar potez)) (list (- (caadr potez) 1) (cadadr potez)) (caddr potez))
)

;;(smanjiredupotezu potez)
;; smanjuje vrednost reda poteza *((8 4) (7 5) 5)* -> *((7 4) (6 5) 5)*


(defun odigraj (tabla potez prenos)
(cond((and (< (caar potez) 1) (< (caadr potez) 1)) tabla)
     ((eq (caar potez) 1) (cons (skloniSaPolja (car tabla) (car potez) (length prenos)) (odigraj (cdr tabla) (smanjiRedUPotezu potez) prenos)))
     ((eq (caadr potez) 1) (cons (dodajNaPolje (car tabla) (cadr potez) prenos) (odigraj (cdr tabla) (smanjiRedUPotezu potez) prenos)))
     (t (cons (car tabla) (odigraj (cdr tabla) (smanjiRedUPotezu potez) prenos)))
))

;;(odigraj tabla potez prenos)
;;prima 3 argumenta 1.tabla, 2.potez u obliku *((7 4) (6 5) 5)*, 3.prenos u obliku liste (x o x o)
;;vraca tablu sa novonastalim stanjem


(defun dodajNaPolje (red polje prenos)
(cond((eq (cadr polje) 0) red)
      ((eq (cadr polje) 1) (cons (dodaj (car red) prenos) (cdr red)))
      (t (cons (car red) (dodajNaPolje (cdr red) (list (car polje) (- (cadr polje) 1)) prenos)))
))

;;(dodajNapolje red polje prenos)
;;prima tri argumenta 1.jedan red tabla 2.polje u tom redu 3.listu 
;;vraca red tabele sa azuriranim poljem


(defun dodaj (stek prenos)
(dopuni (append prenos (remove '- stek))))

;;(dodaj stek prenos)
;;dva argumenta jedan je stek koji se dopunjuje (- - - - - x x o x) drugi je prenos koji se dodaje (x o x)
;;vraca stek sa novim stanjem (- - x o x x x o x)

(defun dopuni (polje)
(cond((eq (length polje) 9) polje)
      (t (dopuni (cons '- polje)))))
;;(dopuni polje)
;;jedan argument polje bez "-" *(x o o x o)*
;;vraca polje duzine 9 sa "-" karakterom *(- - - - x o o x o)*

(defun skloniSaPolja (red polje br)
(cond((eq (cadr polje) 0) red)
      ((eq (cadr polje) 1) (cons (obrisi (car red) br) (cdr red)))
      (t (cons (car red) (skloniSaPolja (cdr red) (list (car polje) (- (cadr polje) 1)) br)))
))

;;(sklonisapolja red polje br)
;;prima tri argumenta prvi je red tabla *((polje)(polje)....)* drugi argument je tacno polje sa koga se uklanjaju igrac *(1 2)*
;;treci argument je broj plocica koje se uklanjaju *4*
;;vraca jedan red tabla sa azuriranim jednim poljem sa koga su skinute plocice 
;;(skloniSaPolja '( NIL (- - - - - - - - -) NIL (- - O O O X O O X) NIL (- - - - - - - - X) NIL (- - - - - - - - X)) '( 1 4) '3) test primer 


(defun obrisi (polje br)
(cond ((eq br 0) polje)
      ((eq (car polje) '-) (append '(-) (obrisi (cdr polje)  br))) 
      (t (append '(-) (obrisi (cdr polje) (- br 1)))))
)

;;(obrisi polje br)
;;prima dva argumenta polje i br=broj plocica koje treba da se sklone sa polja
;;vraca polje bez skinutih plocica *((- - - x x o o x x) 4)* -> *(- - - - - - - x x)*




(defun nadjiPolja (duzina k)
(cond((> k duzina) '())
     ((eq 0 (mod k 2)) (append (nadjiBrojeve k duzina 2) (nadjiPolja duzina (1+ k))))
     (t (append (nadjiBrojeve k duzina 1) (nadjiPolja duzina (1+ k))))
)
)


(defun nadjiBrojeve (brojV duzina ind)
(cond ((> ind duzina) '())
      (t (cons (list brojV ind) (nadjiBrojeve brojV duzina (+ 2 ind))))
))


(defun postojiNaPoljuPlocicaOdIgraca (zapisPolja tabla igrac) 
(let ((POLJE (vratiPoljeMatrice (car zapisPolja) (cadr zapisPolja) tabla) ))
(and (< 0 (length (remove '- POLJE))) (member igrac POLJE ))
)
)

;; (postojiNaPoljuPlocicaOdIgraca zapisPolja tabla igrac)
;; trazi da li na zadatom polju postoji plocica tog igraca 
;; vraca vrednost T ako na polju postoji igraceva plocica

(defun listaKorisnikovihPolja (tabla igrac listaPolja)
(cond((null listaPolja) '())
     ((postojiNaPoljuPlocicaOdIgraca (car listaPolja) tabla igrac) (append (list (car listaPolja)) (listaKorisnikovihPolja tabla igrac (cdr listaPolja) )))
     (t (listaKorisnikovihPolja tabla igrac (cdr listaPolja) ))
)
)
;; (listaKorisnikovihPolja tabla igrac listaPolja)
;; vraća listu svih polja na kojima ima korisnikovih figura u obliku ((2 3)(3 5)(4 7))
