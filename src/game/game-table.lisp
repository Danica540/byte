;;; Funkcije za kreiranje i transformisanje table

(defun napraviTablu (brojVrsta brojKolona)
(cond
    ((eq brojVrsta 0) '())
    ( (eq brojVrsta 1) (append (list (reverse (napraviVrstu brojKolona '-))) (napraviTablu (- brojVrsta 1) brojKolona)))
    ((eq brojVrsta brojKolona) (append (list (napraviVrstu brojKolona '-)) (napraviTablu (- brojVrsta 1) brojKolona)))
    ((eq (mod brojVrsta 2) 1) (append (list (reverse (napraviVrstu brojKolona 'X))) (napraviTablu (- brojVrsta 1) brojKolona)))
    (t (append (list (napraviVrstu brojKolona 'O)) (napraviTablu (- brojVrsta 1) brojKolona)))
))

(defun napraviVrstu (brojKolona simbol)
(cond
    ((eq brojKolona 0) '())
    ((eq (mod brojKolona 2) 0) (append (list (napraviCrnoPolje 9 simbol)) (napraviVrstu (- brojKolona 1) simbol)))
    (t (append (list NIL) (napraviVrstu (- brojKolona 1) simbol)))
))

(defun napraviCrnoPolje (visinaSteka simbol)
(cond 
    ((eq visinaSteka 0) '())
    ((eq visinaSteka 1) (append (list simbol) (napraviCrnoPolje (- visinaSteka 1) simbol)))
    (t (append (list '-) (napraviCrnoPolje (- visinaSteka 1) simbol)))
))

;; Funkcija za pribavljanje polja
(defun vratiPoljeMatrice (vrsta kolona matrica)
(cond
    ((null matrica) '())
    ((eq vrsta 1) (vratiPoljeUVrsti kolona (car matrica))) ;; to je ta vrsta
    (t (vratiPoljeMatrice (- vrsta 1) kolona (cdr matrica)))
))

(defun vratiPoljeUVrsti (kolona vrsta)
(cond
    ((null vrsta) '()) ;; nije polje sa indeksom kolone nadjeno u vrsti
    ((eq kolona 1) (car vrsta)) ;; vrati polje ako je to ta kolona
    (t (vratiPoljeUVrsti (- kolona 1) (cdr vrsta)))
))

