;;; Glavna funkcije za stampanje table
;; Tabla se prvo normalizuje pa transformise za stampanje
;; Parametar koji se prosledjuje ovoj funkciji je obicna reprezentacija table (ne normalizovana, niti transformisana)
;; Ona vrsi normalizaciju i transformaciju matrice, i takvu matricu salje na stampanje pomocnoj funkciji --stampajTablu
(defun stampajTablu (tabla)
(--stampajTablu (transformisiTabluZaStampanje(normalizujTabluZaStampanje tabla))))

;; Pomocna funkcija za stampanje table, prvo stampa brojeve kolona, pa novi red, pa vrste
;; Prosledjuje joj se transformisana tabla, spremna za stampanje
(defun --stampajTablu (tabla)
(progn 
    (format t "  ") 
    (stampajInlinePoruku (stampajZaglavljeZaKolone (progres (length tabla) 4) (reverse (generisiListu (* 4 (length tabla)))))) 
    (format t "~%") 
    (--stampajVrste tabla 0 0))
)

;; U ovoj funkciji je svaka vrsta table indeksirana indeksVrste promenljivom
;; svaka vrsta ovakve table ima 3 reda vrste, ta 3 red ase indeksiraju parametrom indeks, koji uzima vrednosti 0, 1 i 2
(defun --stampajVrste (tabla indeks indeksVrste)
(cond
    ((null tabla) " ")
    (t (progn 
            (--stampajVrstu (car tabla) indeks indeksVrste)
            (--stampajVrste (cdr tabla) indeks (+ indeksVrste 1))))
))

;; Funkcija koja stampa svaki red vrste, pri tome ako je indeks 1 stampa slovo vrste
;; indeks je promenljiva koja uzima vrednosti 0, 1 i 2 i koristi se za numerizaciju redova vrste
;; Svaki red vrste se prosledjuje funkciji stampajPolje
(defun --stampajVrstu (vrsta indeks indeksVrste)
(cond
    ((null vrsta) " ")
    (t(progn 
            (if (eq (mod indeks 3) 1) (format t "~a " (vratiSlovoIndeksaVrste indeksVrste)) (format t "  "))
            (--stampajPolje (car vrsta))
            (--stampajVrstu (cdr vrsta) (+ indeks 1) indeksVrste)))
))

;; Polje sadrzi 1 red vrste
(defun --stampajPolje (polje)
(progn  
    (mapcar '--stampajVrednostPolja polje) 
    (format t "~%")))

(defun --stampajVrednostPolja (redPolja)
(cond
    ((eq nil (car redPolja)) (format t "       "))
    (t (format t " ~a ~a ~a " (car redPolja) (cadr redPolja) (caddr redPolja)))
))

(defun transformisiTabluZaStampanje (tabla)
(mapcar '--transformisiVrstuZaStampanje tabla))

(defun --transformisiVrstuZaStampanje (vrsta)
(append 
    (list (mapcar (lambda (polje) (nth 0 polje)) vrsta)) 
    (append 
        (list (mapcar (lambda (polje) (nth 1 polje)) vrsta)) 
        (list (mapcar (lambda (polje) (nth 2 polje)) vrsta)))))

(defun normalizujTabluZaStampanje (tabla)
(mapcar '--normalizujVrstuZaStampanje tabla))

(defun --normalizujVrstuZaStampanje (vrsta)
(mapcar '--razbijNaTri vrsta))

(defun --razbijNaTri (polje)
(cond 
    ((eq polje nil) '(nil nil nil))
    ((null (cdddr polje)) (list polje))
    (t (append (list (append (append (list (car polje)) (list (cadr polje))) (list (caddr polje)))) (--razbijNaTri (cdddr polje))))
))

(defun vratiSlovoIndeksaVrste (broj)
(code-char (+ (char-code #\A)  broj)))

;ova funkcija je dodata jer vratiSLovoIndeksaVrste vraca #\C npr i to je zgogno samo kod stampanja
(defun vratiSlovoVrste (broj)
(case broj
    (1 'A)
    (2 'B)
    (3 'C)
    (4 'D)
    (5 'E)
    (6 'F)
    (7 'G)
    (8 'H)
    (9 'I)
    (10 'J)
    (11 'K)
    (12 'L)
    (12 'M)
    (14 'N)
    (15 'O)
    (16 'P)
    (17 'Q)
    (18 'R)
    (19 'S)
    (20 'T)
    (21 'U)
    (22 'V)
    (23 'W)
    (24 'X)
    (25 'Y)
    (26 'Z) 
)
)

(defun vratiBrojIndeksaVrste (slovo)
(case slovo
    (A '1)
    (B '2)
    (C '3)
    (#\D '4)
    (E '5)
    (F '6)
    (G '7)
    (H '8)
    (I '9)
    (J '10)
    (K '11)
    (L '12)
    (M '13)
    (N '14)
    (O '15)
    (P '16)
    (Q '17)
    (R '18)
    (S '19)
    (#\T '20)
    (U '21)
    (V '22)
    (W '23)
    (X '24)
    (Y '25)
    (Z '26)
    (t '8)
))

(defun vratiIndeksKolone (key)
(case key
    (1 '1)
    (5 '2)
    (9 '3)
    (13 '4)
    (17 '5)
    (21 '6)
    (25 '7)
    (29 '8)
    (33 '9)
    (37 '10)
    (41 '11)
    (45 '12)
    (49 '13)
    (53 '14)
    (57 '15)
    (61 '16)
    (65 '17)
    (69 '18)
    (73 '19)
    (77 '20)
    (81 '21)
    (85 '22)
    (89 '23)
    (93 '24)
    (97 '25)
))

(defun stampajZaglavljeZaKolone (lista1 lista2)
(cond
    ((null lista1) " ")
    ((eq (car lista1) (car lista2)) (progn (format t "  ~a " (vratiIndeksKolone (car lista1))) (stampajZaglavljeZaKolone (cdr lista1) (cdr lista2))))
    (t (progn (format t " ") (stampajZaglavljeZaKolone lista1 (cdr lista2))))
))