;; Funkcije za proveru pobede

(defun izracunajBrojStekova (dimenzija)
(/ (izracunajBrojFigura dimenzija) 8))

(defun izracunajBrojFigura (dimenzija)
(/ (* dimenzija (- dimenzija 2)) 2))

;; Funkcija koja proverava da li je stek popunjen
(defun daLiJeStekPopunjen (stek)
(cond 
    ((eq (length (remove '- stek)) 8) T)
    (t NIL)
))


(defun skloni (tabla polje) 
(cond ((eq (length (remove '- (vratiPoljeMatrice (car polje) (cadr polje) tabla))) 8) (ocistiPolje tabla polje))
        (t tabla))
)

;;skloni odredjuje da li imamo pun stek koga treba ukloniti sa polja
;;vraca tablu sa novim stanjem
(defun ocistiPolje (tabla polje)
(cond((eq (car polje) 1) (cons (skloniSaPolja (car tabla) polje 8) (cdr tabla)))
(t (cons (car tabla) (ocistiPolje (cdr tabla) (list (- (car polje) 1) (cadr polje))) ))
))

;;postavlja vrednost polja na prazno
(defun promeniSkor (tabla skor polje)
(let ((stek (vratiPoljeMatrice (car polje) (cadr polje) tabla)))
(cond ((eq (length (remove '- stek)) 8) (noviSkor (cadr stek) skor))
(t skor))
))

(defun noviSkor (figura skor)
(cond((eq figura 'x) (list (1+ (car skor)) (cadr skor)))
(t (list (car skor) (1+ (cadr skor))))
))

(defun div (x y) 
(
/(- x (mod x y)) y
))


