;; Validacione funkcije

(require "./game-display.lisp")
(require "./game-table.lisp")
(require "./game-moves.lisp")
(require "./game-win-checks.lisp")

(defun validacijaDimenzija (dimenzija)
(cond
    ((and (eq (mod (izracunajBrojFigura dimenzija) 8) 0) (>= dimenzija 8)) T)
    (t NIL)
))


(defun validacijaZapisaPoteza (potez)
(cond
    ((not (listp potez)) NIL) 
    ((or (< (length potez) 2) (> (length potez) 3)) NIL)
    ( (not (and (listp (car potez)) (listp (cadr potez)))) NIL) 
    ((or (not (eq (length (car potez)) 2)) (not (eq (length (cadr potez)) 2))) NIL) 
    (t T)
))

(defun uGranicamaTable (indeks dimenzija)
(cond ((null indeks) NIL) 
(t(if (and (not(> indeks dimenzija)) (> indeks 0)) t NIL))
))


(defun daLiJePoljeCrno (vrsta kolona)
(= (mod (+ vrsta kolona) 2) 0)
)

(defun validacijaIndeksa (zapisPolja tabla)
(and (uGranicamaTable (vratiBrojIndeksaVrste (car zapisPolja)) (length tabla)) (uGranicamaTable (cadr zapisPolja) (length tabla)) 
(daLiJePoljeCrno (vratiBrojIndeksaVrste (car zapisPolja)) (cadr zapisPolja))
) 
)

(defun postojiIgracNaPolju (zapisPolja tabla igrac) 
(let ((POLJE (vratiPoljeMatrice (vratiBrojIndeksaVrste (car zapisPolja)) (cadr zapisPolja) tabla) ))
(and (< 0 (length (remove '- POLJE))) (member igrac POLJE ) )
)
)

(defun daLiJePoljePrazno (polje tabla)
(let ((POLJE (vratiPoljeMatrice (car polje) (cadr polje) tabla) ))
(and (zerop (length (remove '- POLJE))) (not (null POLJE)))
)
)

(defun validacijaPoljaUPotezu (potez tabla igrac) 
(if   (and (validacijaIndeksa (car potez) tabla) (validacijaIndeksa (cadr potez) tabla) (postojiIgracNaPolju (car potez) tabla igrac))
            (let ((VALIDNIPOTEZI (vratiValidnePoteze tabla (vratiBrojIndeksaVrste (caar potez)) (cadar potez))))
                (if VALIDNIPOTEZI (poljeNaDijagonali potez tabla VALIDNIPOTEZI) '-1  )
            )

            '()

))

(defun proveriDaLiJeDeoPuta  (polje put)
(cond ((null put) '())
      ((equalp polje (car put)) T)
      (t (proveriDaLiJeDeoPuta polje (cdr put)))))
      

(defun poljeNaDijagonali (potez tabla VALIDNIPOTEZI)
(let ((PUTDOPOLJA (append (list (konvertujIndeksPolja (cadr potez ))) (list '1))) )
      (if(proveriDaLiJeDeoPuta PUTDOPOLJA VALIDNIPOTEZI) T '())
))


(defun nadjiNajnizuPlocicuIgraca (pozicija polje igrac) 
(cond ((null polje) '())
((equalp (car polje) igrac) pozicija)
(t(nadjiNajnizuPlocicuIgraca (1+ pozicija) (cdr polje) igrac))
)
)

(defun proveriPlocicu (pozicija polje igrac)
(equalp igrac (nth pozicija (reverse polje))) 
)


(defun noviPotez (stariPotez novaVisina)
(list (car stariPotez) (cadr stariPotez) novaVisina)
)
 
(defun proveriVelicineStekova (potez POLJE DRUGOPOLJE)
(cond ((and (zerop (caddr potez)) (zerop (length (remove '- DRUGOPOLJE)))) potez)
      ((or (>  (1+ (caddr potez)) (length (remove '- DRUGOPOLJE))) (and (not (zerop (caddr potez))) (zerop (length (remove '- DRUGOPOLJE))))) NIL)
      ((> (+ (- (length (remove '- POLJE)) (caddr potez))  (length (remove '- DRUGOPOLJE))) 8) NIL)
      (t potez)
)
)

(defun ispravanPotez (potez igrac tabla)
(let ((POLJE (vratiPoljeMatrice (vratiBrojIndeksaVrste (caar potez )) (cadar potez) tabla))
      (DRUGOPOLJE (vratiPoljeMatrice (vratiBrojIndeksaVrste (caadr potez)) (cadadr potez) tabla)))

(cond ((= (length potez) 2) (proveriVelicineStekova (noviPotez potez (nadjiNajnizuPlocicuIgraca 0 (reverse POLJE) igrac)) POLJE DRUGOPOLJE)) 
      ((or 
        (< (caddr potez) 0) 
        (> (caddr potez) (length (remove '- POLJE))) 
        (not (proveriPlocicu (caddr potez) POLJE igrac ))
       ) '())
      ((eq (proveriVelicineStekova potez POLJE DRUGOPOLJE) nil) '())
      (:else potez)
)))

;nalazi sve visine u okviru steka na kojima je plocica igraca
(defun nadjiSvePlociceIgraca (igrac polje pozicija)
(cond ((null polje) '())
      ((equalp (car polje) igrac) (append (list pozicija) (nadjiSvePlociceIgraca igrac (cdr polje) (1+ pozicija))))
      (t (nadjiSvePlociceIgraca igrac (cdr polje) (1+ pozicija)))
))



;ako mi je polje prazno, onda mogu samo ceo stek da prebacim, pa proverim da li je na visini nula moj igrac, inace moram da proverim za sve visine
(defun napraviSveIspravnePoteze (polje odredista igrac tabla)
(cond ((null odredista) '())
      ;ako na odredistu nema igraca, mogu da se pomerim samo sa nulte visine
      ( (and
            (daLiJePoljePrazno (caar odredista) tabla) 
            (proveriPlocicu 0 (vratiPoljeMatrice (car polje) (cadr polje) tabla) igrac )
        ) 
        (append (list (list (list (vratiSlovoVrste (car polje)) (cadr polje)) 
                                  (list (vratiSlovoVrste (caaar odredista)) (cadaar odredista)) 0) ) 
                (napraviSveIspravnePoteze polje (cdr odredista) igrac tabla))
      )
      ;ako je polje prazno, a moja plocica nije nulta, pravi naredni potez, ovo preskoci!
      ((daLiJePoljePrazno (caar odredista) tabla) (napraviSveIspravnePoteze polje (cdr odredista) igrac tabla))
      (t (append 
           (napraviPotezeDoOdredista polje (nadjiSvePlociceIgraca igrac (reverse (vratiPoljeMatrice (car polje) (cadr polje) tabla)) 0) 
                                     (caar odredista) igrac tabla)
           (napraviSveIspravnePoteze polje (cdr odredista) igrac tabla)
         )
      )
)
)

;ako je ispravan potez, dodati ga u listu i nastaviti rekurzivno
;inace idemo dalje
(defun napraviPotezeDoOdredista ( polje visine odrediste igrac tabla)
(cond ((null visine) '())
      ( (ispravanPotez (list (list (vratiSlovoVrste (car polje)) (cadr polje)) (list (vratiSlovoVrste (car odrediste)) (cadr odrediste)) (car visine)) igrac tabla ) 
        (append 
            (list (list (list (vratiSlovoVrste (car polje)) (cadr polje)) (list (vratiSlovoVrste (car odrediste)) (cadr odrediste)) 
            (car visine)) )

            (napraviPotezeDoOdredista polje (cdr visine) odrediste igrac tabla))
      )
      (t (napraviPotezeDoOdredista polje (cdr visine) odrediste igrac tabla))
)
)

;kao argument dobija listu svih polja sa kojih igrac moze da igra i za svako od tih polja izgenerise poteze
(defun nadjiPotezeZaSvaPopunjenaPolja (polja igrac tabla)
(cond ((null polja) '())
      (t (append 
            (napraviSveIspravnePoteze (car polja) (vratiValidnePoteze tabla (caar polja) (cadar polja)) igrac tabla )
            (nadjiPotezeZaSvaPopunjenaPolja (cdr polja) igrac tabla)
         )
      )
)
)

;za svaki moguci potez izgenerise novu tablu
(defun izgenerisiSvaStanja (tabla igrac potezi) 
(cond ((null potezi) '())
      (t (append 
            (list (odigraj tabla (konvertujpotez (car potezi)) (prenos tabla (konvertujpotez (car potezi)))))
            (izgenerisiSvaStanja tabla igrac (cdr potezi))
         )
      )
)
)

(defun odigrajSvePoteze (tabla igrac)
   (izgenerisiSvaStanja tabla igrac 
                        (nadjiPotezeZaSvaPopunjenaPolja (listaKorisnikovihPolja tabla igrac (nadjiPolja (length tabla) 1)) igrac tabla)
   )
)

(defun vratiSvePotezeSaTable (tabla igrac)
(nadjiPotezeZaSvaPopunjenaPolja (listaKorisnikovihPolja tabla igrac (nadjiPolja (length tabla) 1)) igrac tabla))