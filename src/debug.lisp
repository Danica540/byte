(require "./game/game.lisp")
(require "./helpers/helpers.lisp")
(require "./game/game-validations.lisp")
(require "./game/game-display.lisp")
(require "./game/game-win-checks.lisp")
(require "./game/game-moves.lisp")
(require "./game/inference-engine.lisp")

(setq *tablaProvera* '(
    ((- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL)
    (NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -))
    ((- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL)
    (NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -))
    ((- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - x x o x o o) NIL)
    (NIL (- - - - - - - - -) NIL (- - - x x o o o o) NIL (- - - - - - - - -) NIL (- - - - - - - - -))
    ((- - - - - - - - -) NIL (- - - - - - - o o) NIL (- - - - - - - - o) NIL (- - - - - - - - -) NIL)
    (NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - x o) NIL (- - - - - - - - -))
))

(setq *tabla* '(
    ((- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL)
    (NIL (- - - - - - - - X) NIL (- - - - - - - - X) NIL (- - - - - - - - X) NIL (- - - - - - - - X))
    ((- - - - - - - - O) NIL (- - - - - - - - O) NIL (- - - - - - - - O) NIL (- - - - - - - - O) NIL)
    (NIL (- - - - - - - - X) NIL (- - - - - - - - X) NIL (- - - - - - - - X) NIL (- - - - - - - - X))
    ((- - - - - - - - O) NIL (- - - - - - - - O) NIL (- - - - - - - - O) NIL (- - - - - - - - O) NIL)
    (NIL (- - - - - - - - X) NIL (- - - - - - - - X) NIL (- - - - - - - - X) NIL (- - - - - - - - X))
    ((- - - - - - - - O) NIL (- - - - - - - - O) NIL (- - - - - - - - O) NIL (- - - - - - - - O) NIL)
    (NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -) NIL (- - - - - - - - -))
))

(setq *potez* '((h 6) (g 5)  ))
(setq *igrac* 'o)

;(stampanjePoruke (vratiSvePotezeSaTable *tabla* 'O))

;(stampanjePoruke (vratiValidnePoteze *tablaProvera* '3 '3))
;(trace proveriVelicineStekova)

;(stampanjePoruke (proveriVelicineStekova *potez* (vratiPoljeMatrice '4 '4 *tablaProvera*) (vratiPoljeMatrice '3 '3 *tablaProvera*)))

;(stampanjePoruke (eq (proveriVelicineStekova *potez* (vratiPoljeMatrice '4 '4 *tablaProvera*) (vratiPoljeMatrice '3 '3 *tablaProvera*)) NIL))

;(trace vratiValidnePotezeNaOsnovuNajkracegRastojanja)
;(stampanjePoruke (vratiValidnePoteze *tablaProvera* 3 3))
;(stampanjePoruke (setq *odredista* (vratiValidnePoteze *tablaProvera* (vratiBrojIndeksaVrste (caar *potez*)) (cadar *potez*))))

;; (stampanjePoruke (nadjiPotezeZaSvaPopunjenaPolja (listaKorisnikovihPolja *tablaProvera* *igrac* (nadjiPolja (length *tablaProvera*) 1)) *igrac* 
;; *tablaProvera*))


;(stampanjePoruke (proveriDaLiPostojiStekIgraca *tablaProvera* 'X))
;(stampanjePoruke (nth 1 '(1 2 3 4 5 6 7 8 9)))
;(stampanjePoruke (izracunajBrojVrsnihFigura *tablaProvera* 'o))
;(stampanjePoruke (prepare-knowledge (set-rules) (set-facts *tablaProvera* 'O) 10))
;(stampanjePoruke (infer '(Evaluate ?value 'victory 'O)))
;(stampanjePoruke (proveriSusede *tablaProvera* 'x))
;(stampanjePoruke (vratiSvePotezeSaTable *tablaProvera* 'O))
;(stampanjePoruke (odigrajSvePoteze *tablaProvera* 'o))
;(stampanjePoruke (evaluate *tablaProvera* 'X))

;;(trace proveriTrenutnoStanje)
;;(trace vratiEvaluacijuSuseda)
;(stampanjePoruke (cadr (minmaxAB *tablaProvera* 6 -999999 999999 'x 'x)))

;(stampanjePoruke (vratiEvaluacijuSuseda (odigrajSvePoteze *tablaProvera* (vratiSledecegIgraca 'o)) 'o))

(defun main ()
(let ((DIMENZIJA (unosKorisnika "Unesite dimenzije tabele (8 ili 10):")))
(cond
    ((eq (validacijaDimenzija DIMENZIJA) T) 
    (progn 
        (let ((KORISNOKOVIGRAC (unosKorisnika "Izaberite svog igraca (X igrac igra prvi, O igrac igra drugi):")))
             (if (or (eq KORISNOKOVIGRAC 'X) (eq KORISNOKOVIGRAC 'O)) 
                (igra *tablaProvera* '(0 0) KORISNOKOVIGRAC 'X) 
            (progn (stampanjePoruke "Nevalidan unos igraca.") (main)))
        )))
    (t (progn (stampanjePoruke "Dimenzije nevalidne.") (main)))
)
))

(main)



